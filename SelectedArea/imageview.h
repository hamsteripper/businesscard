#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QDebug>
#include <QDate>
#include <QDateTime>
#include <QCoreApplication>
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include "SelectedArea/mydialog.h"

class imageView: public QWidget{

    Q_OBJECT
public:
    imageView(QWidget * parent = 0);
    ~imageView();

private:
    bool exit;
    int x0, y0, x1, y1;
    QRect rectSelectedArea;
    bool mousePress;
    QImage *img;
    QImage *newImg;
    void newDialog(void);
    MyDialog *dialog;
    QPoint offset;

    bool square = false;
    bool rectangle_3x4 = false;
    bool random = true;

signals:
    void areaIsSelected(void);
    void windowClose(void);
    void sendImg(QImage);

public slots :
    void setImage(QImage img);
    void menuSetChecked(QString);

private slots:
    void yesOrNo(bool b);

protected:
    void paintEvent(QPaintEvent *);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *);
    void keyPressEvent(QKeyEvent *event);
};

#endif // IMAGEVIEW_H
