#include "SelectedArea/imageview.h"

imageView::imageView(QWidget * parent) : QWidget(parent)
{
    this->setWindowTitle("Select Area");

    mousePress = false;
    x0 = 0;
    y0 = 0;
    x1 = 0;
    y1 = 0;
    //    exit = false;
    this->setFocusPolicy(Qt::StrongFocus);
}

imageView::~imageView(){}

void imageView::menuSetChecked(QString checkbox){
    if(checkbox == "square"){
        square = true;
        rectangle_3x4 = false;
        random = false;
    }else if(checkbox == "rectangle_3x4"){
        square = false;
        rectangle_3x4 = true;
        random = false;
    }else if(checkbox == "random"){
        square = false;
        rectangle_3x4 = false;
        random = true;
    }
}

void imageView::setImage(QImage img){
    this->img = new QImage(img);
    this->newImg = this->img;
    this->resize(img.width(), img.height());
}

void imageView::paintEvent(QPaintEvent *)
{

    QPainter painter(this);

    painter.setPen(Qt::black);
    painter.drawImage(0,0,*newImg);
    painter.drawRect(rect());

    QPen pen;
    pen.setColor(Qt::white);
    pen.setStyle(Qt::DashLine);
    painter.setPen(pen);
    painter.setBrush(QColor(100, 100, 100, 0));
    painter.drawRect(x0, y0, x1-x0, y1-y0);

    QString textHint;
    textHint = tr("Для снимка выделите область и нажмите SPACE");

    QFont font;
    font.setPixelSize(30);

    painter.setFont(font);
    painter.setPen(Qt::white);

    painter.drawText(rect(), Qt::AlignCenter, textHint);

    //

}
void imageView:: keyReleaseEvent(QKeyEvent *event){
    event->ignore();
}

void imageView:: keyPressEvent(QKeyEvent *event){
    // Закрыть widget
    if(event->key() == Qt::Key_Escape){
        exit = false;
        close();
    }
}

void imageView::newDialog(){
    dialog = new MyDialog();
    connect(dialog, SIGNAL(signalButtonClick(bool)), this, SLOT(yesOrNo(bool)));
    // Модальное открытие окна
    if (dialog->exec() == QDialog::Accepted){}
    // Если обрезанное изображение устраивает, то выход, иначе продолжить
    if(exit){
        close();
    }else{
        newImg = img;
    }
    delete dialog;
}

void imageView::yesOrNo(bool b){
    exit = b;
}

void imageView:: mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton){
        offset = event->pos();
        mousePress = true;
        x0 = event->x();
        y0 = event->y();
        x1 = event->x();
        y1 = event->y();

        setCursor(QCursor(Qt::CrossCursor));
    }
}

void imageView:: mouseReleaseEvent(QMouseEvent *event)
{
    mousePress = false;
    setCursor(QCursor(Qt::ArrowCursor));

    if(event->button() == Qt::MiddleButton){
        close();
        emit areaIsSelected();
    }

    //    x1 = event->x();
    //    y1 = event->y();

    if(x0 > x1){
        int temp = x0;
        x0 = x1;
        x1 = temp;
    }

    if(y0 > y1){
        int temp = y0;
        y0 = y1;
        y1 = temp;
    }


    QRect rect(x0, y0, x1-x0, y1-y0);
    // Вырезать прямоугольник из изображения
    newImg = new QImage(img->copy(rect));
    exit = true;
    // Отправить вырезанное изображение
    newDialog();
}

void imageView::mouseMoveEvent(QMouseEvent *event)
{
    //QPoint difference = event->pos() - offset;

    if(square){
        if(event->buttons() & Qt::LeftButton){
            if(x1 != event->x()){
                int xx = event->x() - x1;
                x1 = x1 + xx;
                y1 = y1 + xx;
            }
            update();
        }
    }else if (rectangle_3x4){
        if(x1 != event->x()){
            int xx = event->x() - x1;
            x1 = x1 + xx;
            double dx = x1 - x0;
            y1 = y0 + (dx / 3) * 4;
        }
        update();
    }else if (random){
        x1 = event->x();
        y1 = event->y();
        update();
    }
}

void imageView:: closeEvent(QCloseEvent *event)
{
    if (exit) {
        emit sendImg(*newImg);
        event->accept();
    } else {
        event->ignore();
    }
}
