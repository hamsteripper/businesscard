#ifndef MYDIALOG_H
#define MYDIALOG_H

#include <QDialog>
#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSignalMapper>
#include <QDebug>

class MyDialog : public QDialog
{
    Q_OBJECT

public:
    MyDialog(QWidget * parent = 0);

private:
    QVBoxLayout *layout;
    QPushButton *yes;
    QPushButton *no;
    QSignalMapper *signalMapper;

signals:
    void signalButtonClick(bool);

private slots:
    void slotButtonClick(QString);

};

#endif // MYDIALOG_H
