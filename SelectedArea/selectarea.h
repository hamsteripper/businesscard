#ifndef SELECTAREA_H
#define SELECTAREA_H

/*
 Этот класс наследуется от QWidget
 создает окно без рамки
 окно на весь экран и прозрачно на половину
 на этом окне можно выделять область
 при нажатии пробела, виджет подает сигнал и закрывается

 связываю этот класс с классом ScreenShot
 когда поступает сигнал, класс ScreenShot его отлавливает и
 получает координаты выделенной области, после чего делает по
 ним скриншот области экрана
 */
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QDebug>
#include <QDate>
#include <QDateTime>
#include <QCoreApplication>
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include "SelectedArea/mydialog.h"
#include "SelectedArea/imageview.h"
#include <QSizePolicy>
#include <QSignalMapper>
#include <QMenuBar>
#include <QCheckBox>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class SelectedArea; }
QT_END_NAMESPACE

class SelectedArea: public QMainWindow
{
    Q_OBJECT
public:
    SelectedArea(QWidget * parent = 0);
    ~SelectedArea();

private:
    Ui::SelectedArea *ui;
    bool exit;
    int x0, y0, x1, y1;
    QRect rectSelectedArea;
    bool mousePress;
    QImage *img;
    QImage *newImg;
    MyDialog *dialog;
    QPoint offset;
    int photoFormat = 1;
    imageView *imgV;

    QSignalMapper *sigMap;

    QMenuBar* mnuBar;
    QMenu* pmnu;
    QAction *square;
    QAction *rectangle_3x4;
    QAction *random;
    QActionGroup *aGroup;

signals:
    void areaIsSelected(void);
    void windowClose(void);
    void sendImg(QImage);

public slots :
    void setImage(QImage img);

//protected:
    //void closeEvent(QCloseEvent *);

private slots:
    void createConnections();
    //void getImage(QImage);

};

#endif // SELECTAREA_H
