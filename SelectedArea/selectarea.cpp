#include "SelectedArea/selectarea.h"
#include "ui_SelectedArea.h"

SelectedArea::SelectedArea(QWidget * parent) : QMainWindow(parent), ui(new Ui::SelectedArea)
{
    ui->setupUi(this);

    this->setWindowTitle("Select Area");

    mousePress = false;
    x0 = 0;
    y0 = 0;
    x1 = 0;
    y1 = 0;

    exit = false;
    this->setFocusPolicy(Qt::StrongFocus);
    imgV = new imageView(this);

    /****/
    square = new QAction("Квадратное");
    square->setCheckable(true);
    rectangle_3x4 = new QAction("Фото 3x4");
    rectangle_3x4->setCheckable(true);
    random = new QAction("Произвольное");
    random->setCheckable(true);
    random->setChecked(true);
    /****/
    mnuBar = new QMenuBar(this);
    pmnu   = new QMenu("&Формат фото");
    aGroup = new QActionGroup(this);
    aGroup->addAction(square);
    aGroup->addAction(rectangle_3x4);
    aGroup->addAction(random);
    pmnu->addAction(square);
    pmnu->addAction(rectangle_3x4);
    pmnu->addAction(random);
    mnuBar->addMenu(pmnu);
    mnuBar->show();

    setCentralWidget(imgV);
    setMenuBar(mnuBar);
    createConnections();
}

//void SelectedArea::getImage(QImage img){



//}


SelectedArea::~SelectedArea(){}

void SelectedArea::setImage(QImage img){

    imgV->setImage(img);
    imgV->resize(img.width(), img.height());
    this->setFixedSize(imgV->size());

}

void SelectedArea::createConnections(){

    sigMap = new QSignalMapper();

    // подключаем сигнал клика кнопки к мапперу
    connect(square, SIGNAL(triggered()), sigMap, SLOT(map()));
    connect(rectangle_3x4, SIGNAL(triggered()), sigMap, SLOT(map()));
    connect(random, SIGNAL(triggered()), sigMap, SLOT(map()));

    sigMap->setMapping(square, "square");
    sigMap->setMapping(rectangle_3x4, "rectangle_3x4");
    sigMap->setMapping(random, "random");
    connect(sigMap, SIGNAL(mapped(QString)), imgV, SLOT(menuSetChecked(QString)));

    // Получение изображения
    connect(imgV, SIGNAL(sendImg(QImage)), this, SIGNAL(sendImg(QImage)));

}

//void SelectedArea:: closeEvent(QCloseEvent *event)
//{
//    //    if (!exit) {
//    //        emit sendImg(*newImg);
//    //        event->accept();
//    //    } else {
//    //        event->ignore();
//    //    }
//}
