QT += core
QT -= gui
QT += widgets
QT += printsupport

CONFIG += c++11

TARGET = BusinessCard
CONFIG += console
CONFIG -= app_bundle

QT += multimedia multimediawidgets

TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RESOURCES += \
    source/draggableicons.qrc

FORMS += \
    cameraWidget/camerawidget.ui \
    SelectedArea/dialog.ui \
    SelectedArea/form.ui \
    SelectedArea/SelectedArea.ui \
    FirstAndSecondWindows/setCardSize/setcardsize.ui

DISTFILES += \
    source/images/button.png \
    source/images/image.png \
    source/images/lable.png \
    source/images/lineEdit.png \
    source/images/photo.png \
    source/images/printer.png \
    source/images/rebuild.png \
    source/images/stars.png \
    source/images/textEdit.png \
    source/images/tower.png

HEADERS += \
    cameraWidget/camerawidget.h \
    FirstAndSecondWindows/include/cardmainwindow.h \
    FirstAndSecondWindows/include/cardsecondwindow.h \
    FirstAndSecondWindows/include/mycanvas.h \
    include/consoletomultimap.h \
    include/consolewidgetparameters.h \
    include/counterthread.h \
    include/inputoutput.h \
    include/parametersofwidget.h \
    include/rewritexml.h \
    Printer/myprinter.h \
    Printer/rewritePrinterSettings.h \
    SelectedArea/imageview.h \
    SelectedArea/mydialog.h \
    SelectedArea/selectarea.h \
    widgetEditDialog/include/WidgetEditDialog.h \
    Widgets/mylable.h \
    Widgets/mylineedit.h \
    Widgets/mytextedit.h \
    Widgets/widgetCM.h \
    FirstAndSecondWindows/setCardSize/setcardsize.h

SOURCES += \
    cameraWidget/camerawidget.cpp \
    FirstAndSecondWindows/src/cardmainwindow.cpp \
    FirstAndSecondWindows/src/cardsecondwindow.cpp \
    FirstAndSecondWindows/src/mycanvas.cpp \
    Printer/myprinter.cpp \
    Printer/rewritePrinterSettings.cpp \
    SelectedArea/imageview.cpp \
    SelectedArea/mydialog.cpp \
    SelectedArea/selectarea.cpp \
    src/consoletomultimap.cpp \
    src/counterthread.cpp \
    src/rewritexml.cpp \
    widgetEditDialog/src/widgetEditDialog.cpp \
    Widgets/mylable.cpp \
    Widgets/mylineedit.cpp \
    Widgets/mytextedit.cpp \
    Widgets/widgetCM.cpp \
    main.cpp \
    FirstAndSecondWindows/setCardSize/setcardsize.cpp

