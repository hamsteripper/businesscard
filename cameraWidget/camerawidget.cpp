#include "cameraWidget/camerawidget.h"
#include "ui_camerawidget.h"

CameraWidget::CameraWidget(QWidget *parent) : QDialog(parent), ui(new Ui::CameraWidget), camera(0), imageCapture(0), mediaRecorder(0), isCapturingImage(false), applicationExiting(false) {

    ui->setupUi(this);
    // Меню
    menuBar = new QMenuBar();

    // Подменю
    menuFile = new QMenu(this);
    menuDevices = new QMenu("Cameras");
    actionExit = new QAction("Exit",this);
    menuFile->addAction(actionExit);
    // Добавление элемента подменю в меню
    menuBar->addAction(actionExit);
    menuBar->addMenu(menuDevices);
    // Добавления меню в вертикальный виджет
    ui->verticalLayout_3->addWidget(menuBar);
    //menuBar->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    //ui->verticalLayout_3->setMenuBar(menuBar);

    buttonConnectors();

    ///Поиск и добавление найденных устройств (камер) в список
    // Групповые действия
    QActionGroup *videoDevicesGroup = new QActionGroup(this);
    // Только одно отмечаемое действие в группе действий может быть активно в каждый момент времени
    videoDevicesGroup->setExclusive(true);
    // Информация о камере
    foreach (const QCameraInfo &cameraInfo, QCameraInfo::availableCameras()) {
        // videoDevicesGroup родитель videoDeviceAction + идентификатор
        QAction *videoDeviceAction = new QAction(cameraInfo.description(), videoDevicesGroup);
        // Если да, то происходит переключение элементов
        videoDeviceAction->setCheckable(true);
        // Данные о камере
        videoDeviceAction->setData(QVariant::fromValue(cameraInfo));
        // Выбор камеры по умолчанию
        if (cameraInfo == QCameraInfo::defaultCamera())
            videoDeviceAction->setChecked(true);
        // Добавление элемента в меню
        menuDevices->addAction(videoDeviceAction);
    }
    connect(videoDevicesGroup, SIGNAL(triggered(QAction*)), SLOT(updateCameraDevice(QAction*)));
    setCamera(QCameraInfo::defaultCamera());
}

void CameraWidget::buttonConnectors(){
    QObject::connect(actionExit, SIGNAL(triggered()), this, SLOT(close()));
    QObject::connect(ui->takeImageButton, SIGNAL(clicked()), this, SLOT(takeImage()));
}

CameraWidget::~CameraWidget(){
    delete mediaRecorder;
    delete imageCapture;
    delete camera;
}


void CameraWidget::setCamera(const QCameraInfo &cameraInfo){

    delete imageCapture;
    delete mediaRecorder;
    delete camera;

    // Создать класс камера
    camera = new QCamera(cameraInfo);
    connect(camera, SIGNAL(error(QCamera::Error)), this, SLOT(displayCameraError()));

    mediaRecorder = new QMediaRecorder(camera);

    imageCapture = new QCameraImageCapture(camera);

    connect(mediaRecorder, SIGNAL(durationChanged(qint64)), this, SLOT(updateRecordTime()));
    connect(mediaRecorder, SIGNAL(error(QMediaRecorder::Error)), this, SLOT(displayRecorderError()));

    mediaRecorder->setMetaData(QMediaMetaData::Title, QVariant(QLatin1String("Test Title")));

    camera->setViewfinder(ui->viewfinder);
    //ui->viewfinder->setVisible(true);
    connect(imageCapture, SIGNAL(readyForCaptureChanged(bool)), this, SLOT(readyForCapture(bool)));
    connect(imageCapture, SIGNAL(imageCaptured(int,QImage)), this, SLOT(processCapturedImage(int,QImage)));
    connect(imageCapture, SIGNAL(imageSaved(int,QString)), this, SLOT(imageSaved(int,QString)));
    connect(imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), this, SLOT(displayCaptureError(int,QCameraImageCapture::Error,QString)));

    camera->start();

}

void CameraWidget::keyPressEvent(QKeyEvent * event){
    if (event->isAutoRepeat())
        return;

    switch (event->key()) {
    case Qt::Key_CameraFocus:
        displayViewfinder();
        camera->searchAndLock();
        event->accept();
        break;
    case Qt::Key_Camera:
        if (camera->captureMode() == QCamera::CaptureStillImage) {
            takeImage();
        } else {
            if (mediaRecorder->state() == QMediaRecorder::RecordingState)
                stop();
            else
                record();
        }
        event->accept();
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}
void CameraWidget::displayViewfinder(){
    ui->stackedWidget->setCurrentIndex(0);
}
void CameraWidget::takeImage(){
    isCapturingImage = true;
    imageCapture->capture();
}
void CameraWidget::stop(){
    mediaRecorder->stop();
}
void CameraWidget::record(){
    mediaRecorder->record();
    updateRecordTime();
}
void CameraWidget::pause(){
    mediaRecorder->pause();
}
void CameraWidget::startCamera(){
    camera->start();
}
void CameraWidget::stopCamera(){
    camera->stop();
}
void CameraWidget::toggleLock(){
    switch (camera->lockStatus()) {
    case QCamera::Searching:
    case QCamera::Locked:
        camera->unlock();
        break;
    case QCamera::Unlocked:
        camera->searchAndLock();
    }
}

void CameraWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;

    switch (event->key()) {
    case Qt::Key_CameraFocus:
        camera->unlock();
        break;
    default:
        QWidget::keyReleaseEvent(event);
    }
}

void CameraWidget::closeEvent(QCloseEvent *event)
{
    if (isCapturingImage) {
        setEnabled(false);
        applicationExiting = true;
        event->ignore();
    } else {
        event->accept();
    }
}

void CameraWidget::updateRecordTime(){
    QString str = QString("Recorded %1 sec").arg(mediaRecorder->duration()/1000);
}

void CameraWidget::updateCameraDevice(QAction *action)
{
    setCamera(qvariant_cast<QCameraInfo>(action->data()));
}

void CameraWidget::displayCameraError()
{
    QMessageBox::warning(this, tr("Camera error"), camera->errorString());
}


void CameraWidget::displayRecorderError()
{
    QMessageBox::warning(this, tr("Capture error"), mediaRecorder->errorString());
}

void CameraWidget::setExposureCompensation(int index)
{
    camera->exposure()->setExposureCompensation(index*0.5);
}

void CameraWidget::readyForCapture(bool ready)
{
    ui->takeImageButton->setEnabled(ready);
}

void CameraWidget::processCapturedImage(int requestId, const QImage& img)
{
    Q_UNUSED(requestId);
    QImage scaledImage = img.scaled(ui->viewfinder->size(),
                                    Qt::KeepAspectRatio,
                                    Qt::SmoothTransformation);

    QGraphicsScene* scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, 2, 2);
    QPainter painter(&scaledImage);
    scene->render(&painter);

    QString date = QDate::currentDate().toString("dd.MM.yyyy");
    date = date.replace(".","_");
    QString time = QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    time = time.replace(":","_");
    time = time.replace(".","_");

    ui->lastImagePreviewLabel->setPixmap(QPixmap::fromImage(scaledImage));

    selectArea = new SelectedArea(this);
    selectArea->setImage(scaledImage);
    selectArea->setAttribute(Qt::WA_DeleteOnClose);
    selectArea->setWindowFlags(Qt::Dialog);
    selectArea->setWindowModality(Qt::WindowModal);
    selectArea->show();

    connect(selectArea, SIGNAL(sendImg(QImage)), this, SLOT(getImage(QImage)));
}

void CameraWidget::getImage(QImage image){
    emit sendImage(image);
    close();
}

void CameraWidget::displayCapturedImage()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void CameraWidget::imageSaved(int id, const QString &fileName)
{
    Q_UNUSED(id);
    Q_UNUSED(fileName);

    isCapturingImage = false;
    if (applicationExiting)
        close();
}

void CameraWidget::displayCaptureError(int id, const QCameraImageCapture::Error error, const QString &errorString)
{
    Q_UNUSED(id);
    Q_UNUSED(error);
    QMessageBox::warning(this, tr("Image Capture Error"), errorString);
    isCapturingImage = false;
}
