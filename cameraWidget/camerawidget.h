#ifndef CAMERAWIDGET_H
#define CAMERAWIDGET_H

#include <QObject>
#include <QWidget>
#include <QCamera>
#include <QMediaRecorder>
#include <QActionGroup>
#include <QAction>
#include <QCameraInfo>
#include <QVBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QCameraImageCapture>
#include <QMediaMetaData>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QPainter>
#include <QDate>
#include <QDateTime>
#include <QTimer>
#include <QKeyEvent>
#include <SelectedArea/selectarea.h>
#include <QLayout>
#include <QGridLayout>
#include <QImage>

QT_BEGIN_NAMESPACE
namespace Ui { class CameraWidget; }
QT_END_NAMESPACE

Q_DECLARE_METATYPE(QCameraInfo)

class CameraWidget : public QDialog
{
    Q_OBJECT

    QVBoxLayout *boxLayout;
    QMenuBar* menuBar;
    QMenu *menuFile;
    QAction *actionStartCamera;
    QAction *actionStopCamera;
    QAction *action1;
    QAction *actionSettings;
    QAction *action2;
    QAction *actionExit;
    QMenu *menuDevices;
    QLayout *layout;
    SelectedArea *selectArea;

public:
    explicit CameraWidget(QWidget *parent = 0);
    ~CameraWidget();
private:
    QCamera *camera;
    QCameraImageCapture *imageCapture;
    QMediaRecorder* mediaRecorder;

    QImageEncoderSettings imageSettings;
    QAudioEncoderSettings audioSettings;
    QVideoEncoderSettings videoSettings;
    QString videoContainerFormat;
    Ui::CameraWidget *ui;

    bool isCapturingImage;
    bool applicationExiting;

    void buttonConnectors();

signals:
    void sendImage(QImage);

private slots:
    void setCamera(const QCameraInfo &cameraInfo);
    void updateRecordTime();
    void updateCameraDevice(QAction *action);
    void displayCameraError();
    void displayRecorderError();
    void setExposureCompensation(int index);
    void readyForCapture(bool ready);
    void processCapturedImage(int requestId, const QImage& img);
    void displayCapturedImage();
    void imageSaved(int id, const QString &fileName);
    void displayCaptureError(int id, const QCameraImageCapture::Error error, const QString &errorString);
    void displayViewfinder();
    void takeImage();
    void stop();
    void record();
    void pause();
    void startCamera();
    void stopCamera();
    void toggleLock();
    void getImage(QImage);

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);
};

#endif // CAMERAWIDGET_H
