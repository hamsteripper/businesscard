#include "include/consoletomultimap.h"
#include "include/inputoutput.h"

struct keys{
    bool fileOpen = false;
    bool changeSettings = false;
    bool text = false;
    bool image = false;
    bool screenshot = false;
    bool print = false;
    bool camera = false;
    bool exit1 = false;


    clear(){
        fileOpen = false;
        changeSettings = false;
        text = false;
        image = false;
        screenshot = false;
        print = false;
        camera = false;
        exit1 = false;
    }
};

QMultiMap<QString, ConsoleWidgetParameters> readArgs(int argc, char *argv[]){



    setlocale(LC_ALL, "Russian");

    const char* const short_opts = "f:c:u:t:i:s:p:z:m:n:w";

    /*
    0 — не принимает аргумент;
    1 — обязательный аргумент;
    2 — необязательный аргумент.
    */

    const option long_opts[] = {
        {"fileOpen", 2, nullptr, 'f'},
        {"changeSettings", 0, nullptr, 'c'},
        {"userName", 2, nullptr, 'u'},
        {"text", 2, nullptr, 't'},
        {"image", 2, nullptr, 'i'},
        {"screenshot", 0, nullptr, 's'},
        {"print", 2, nullptr, 'p'},
        {"camera", 2, nullptr, 'm'},
        {"camW", 2, nullptr, 'n'},
        {"preview", 0, nullptr, 'w'},
        {"exit", 0, nullptr, 'z'},
        {nullptr, 0, nullptr, 0}
    };

    QMultiMap<QString, ConsoleWidgetParameters> mmap;
    ConsoleWidgetParameters cwp;
    keys key;

    while (true)
    {
        // Чтение нового параметра
        const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
        // Если параметров больше нет, то записать последнюю структуру
        if (-1 == opt){
            // Если есть ещё несохранённый параметр у которого есть имя файла
            if(cwp.userName != "")
                mmap.insert("widget",cwp);
            cwp.clear();
            break;
        }

        switch (opt)
        {
        case 'f': // Если это ключ "открыть файл"
            key.clear();
            key.fileOpen = true;
            cwp.clear();
            cwp.fileXmlPath = QString::fromLocal8Bit(optarg);
            //qDebug() << "File name = " << QString::fromLocal8Bit(optarg);
            mmap.insert("fileOpen",cwp);
            cwp.clear();
            break;
        case 'c': // Если это ключ "изменить параметры"
            if(key.fileOpen){
                cwp.clear();
                cwp.changeSettings = true;
                key.changeSettings = true;
                mmap.insert("changeSettings",cwp);
                qDebug() << "changeSettings = " << QString::fromLocal8Bit(optarg);
                cwp.clear();
            }else{
                cout << "key \"fileOpen\" not found" << endl;
                exit(0);
            }
            break;
        case 'u': // Если это ключ "пользовательское имя виджета"
            if(key.changeSettings){
                // Если структура уже включает какое-то имя, то запомнить структуру и очистить
                if(cwp.userName != ""){
                    mmap.insert("widget",cwp);
                    key.text = false;
                    key.image = false;
                }
                cwp.clear();
                cwp.userName = QString::fromLocal8Bit(optarg);
                qDebug() << "User Name = " << QString::fromLocal8Bit(optarg);
            }else{
                cout << "key \"changeSettings\" not found" << endl;
                exit(0);
            }
            break;
        case 't': // Если это ключ "текст"
            if(cwp.userName != ""){
                cwp.text = QString::fromLocal8Bit(optarg);
                qDebug() << "Text = " << QString::fromLocal8Bit(optarg);
            }else{
                cout << "key \"userName\" not found" << endl;
                exit(0);
            }
            break;
        case 'i': // Если это ключ "изображение"
            if(cwp.userName != ""){
                cwp.imagePath = QString::fromLocal8Bit(optarg);
                qDebug() << "Text = " << QString::fromLocal8Bit(optarg);
            }else{
                cout << "key \"userName\" not found" << endl;
                exit(0);
            }
            break;
        case 's': // Если это ключ "скриншот"
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            if(key.fileOpen){
                cwp.clear();
                cwp.screenshot = QString::fromLocal8Bit(optarg);
                qDebug() << "Screenshot = " << QString::fromLocal8Bit(optarg);
                mmap.insert("screenshot",cwp);
                cwp.clear();
            }else{
                cout << "key \"fileOpen\" not found" << endl;
                exit(0);
            }
            break;
        case 'p': // Если это ключ "Печать"
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            if(key.fileOpen){
                key.print = true;
                cwp.clear();
                cwp.print = QString::fromLocal8Bit(optarg);
                qDebug() << "Print = " << QString::fromLocal8Bit(optarg);
                mmap.insert("print",cwp);
                cwp.clear();
            }else{
                cout << "key \"fileOpen\" not found" << endl;
                exit(0);
            }
            break;
        case 'w': // Если это ключ "Предпросмотр"
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            if(key.fileOpen && key.print){
                cwp.clear();
                key.print = false;
                cwp.preview = true;
                mmap.insert("preview",cwp);
                qDebug() << "Preview = " << QString::fromLocal8Bit(optarg);
                cwp.clear();
            }else{
                cout << "key \"fileOpen\" or \"print\" not found" << endl;
                exit(0);
            }
            break;
        case 'm': // Если это ключ "Камера"
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            if(key.fileOpen && key.changeSettings){
                cwp.clear();
                key.camera = true;
                cwp.camera = QString::fromLocal8Bit(optarg);
                mmap.insert("camera",cwp);
                qDebug() << "Camera = " << QString::fromLocal8Bit(optarg);
                cwp.clear();
            }else{
                cout << "key \"camera\" found, but  \"fileOpen\" or \"changeSettings\" not found" << endl;
                exit(0);
            }
            break;
        case 'n': // Если это ключ "Виджет для камеры"
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            if(key.fileOpen && key.camera){
                cwp.clear();
                key.camera = false;
                cwp.camW = QString::fromLocal8Bit(optarg);
                qDebug() << "camW = " << QString::fromLocal8Bit(optarg);
                mmap.insert("camW",cwp);
                cwp.clear();
            }else{
                cout << "key \"fileOpen\" or \"camera\" not found" << endl;
                exit(0);
            }
            break;
        case 'z':
            if(cwp.userName != ""){
                mmap.insert("widget",cwp);
                key.text = false;
                key.image = false;
            }
            cwp.clear();
            qDebug() << "exit = " << QString::fromLocal8Bit(optarg);
            mmap.insert("exit",cwp);
            cwp.clear();
            break;
        default:
            cout << "error" << endl;
            exit(0);
        }
    }

    return mmap;
}
