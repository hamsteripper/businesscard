#include "include/counterthread.h"

counterThread::counterThread(){

}

void counterThread::setTime(int mSec){
    this->mSec = mSec;
}

void counterThread::run(){

    QTime dieTime= QTime::currentTime().addMSecs(mSec);
    while (QTime::currentTime() < dieTime){}
    emit countStop();
}

void counterThread::stop(){



}
