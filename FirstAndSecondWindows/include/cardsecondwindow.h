#ifndef SECONDWINDOW_H
#define SECONDWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QString>
#include <QTextEdit>
#include <QListWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include "include/inputoutput.h"
#include <QSlider>
#include <QLabel>
#include <QFontComboBox>
#include <QSpinBox>
#include <QFontDialog>
#include "include/parametersofwidget.h"

class CardSecondWindow : public QWidget{
    Q_OBJECT
private:
    QHBoxLayout* frameLayout;
    QListWidget* itemListWidget;
public:
    CardSecondWindow();
private:
    void closeEvent(QCloseEvent *_e);
    void createListWidget();
    void createLayout();
signals:
    void closeCardMainWindow(QCloseEvent *_e);

};

#endif // SECONDWINDOW_H
