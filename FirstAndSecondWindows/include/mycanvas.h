#ifndef MYCANVAS_H
#define MYCANVAS_H

#include <QWidget>
#include <QObject>
#include <QPainter>

class myCanvas : public QWidget{
    Q_OBJECT
private:
    bool saveToImage = false;
    int step = 13;
public:
    myCanvas();
    myCanvas(QWidget *parent);
    void screenStart();
    void screenFinish();
private slots:
    void paintEvent(QPaintEvent *event);
};


#endif // MYCANVAS_H
