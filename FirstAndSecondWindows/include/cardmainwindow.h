#ifndef CARDMAINWINDOW_H
#define CARDMAINWINDOW_H

#include <QApplication>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QObject>
#include <QString>
#include <QTextEdit>
#include <QListWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPoint>
#include <QMimeData>
#include <QPaintEvent>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsSceneDragDropEvent>
#include <QDebug>
#include <QStringList>
#include <QMap>
#include <QFileDialog>
#include <QAction>
#include <QMimeData>
#include <QStandardItemModel>
#include <QScreen>
#include "FirstAndSecondWindows/include/cardsecondwindow.h"
#include "include/inputoutput.h"
#include "Widgets/mylable.h"
#include "Widgets/mytextedit.h"
#include "Widgets/mylineedit.h"
#include "include/rewritexml.h"
#include <QBitmap>
#include <QMenuBar>
#include "FirstAndSecondWindows/include/mycanvas.h"
#include "include/parametersofwidget.h"
#include "widgetEditDialog/include/WidgetEditDialog.h"
#include "include/consolewidgetparameters.h"
#include <QMultiMap>
#include <Printer/myprinter.h>
#include <QThread>
#include "include/counterthread.h"
#include "cameraWidget/camerawidget.h"
#include <QDropEvent>
#include <QPrintDialog>
#include <QWidgetAction>
#include <QLayout>
#include <QGroupBox>
#include <QStatusBar>
#include <FirstAndSecondWindows/setCardSize/setcardsize.h>
#include <QPrinterInfo>

class CardMainWindow : public QMainWindow{
    Q_OBJECT
private:
    // menu
    QMenuBar* mnuBar;
    QMenu* pmnu;

    myCanvas *canvas;

    bool cardIsSaved = false;
    CardSecondWindow *secondWin = NULL;
    WidgetEditDialog *widgetEdit = NULL;
    QPoint m_dragStart;
    QGraphicsScene  *scene;
    QGraphicsView *view;
    int step = 13;
    bool saveToImage = false;
    QList<QMap<QString, QString>> *listMap = NULL;
    QWidgetList *qWLst;
    QAction* load;
    QAction* save;
    QAction* image;
    QAction* print;
    QAction* printPreview;
    QLineEdit *lineW;
    QLineEdit *lineH;

    QPushButton *resizeAccept;
    int currentWidgetNumber = 0;
    QString currentNameOfEditWidget;
    QMultiMap<QString, ConsoleWidgetParameters> *mmap = nullptr;
    bool console = false;
    QPixmap currentImage;
    QDropEvent *currentDropEvent;
    bool createWidgetFromCamera = true;

    counterThread *thread;

public:
    CardMainWindow(QMultiMap<QString, ConsoleWidgetParameters>);
    CardMainWindow();
private:
    QString openCardSecondWindow();
    void createWidgetEditDialog();
    void createConnects(void);
    void closeEvent(QCloseEvent *);
    QPoint positionCalculate(int w, int h);
    int addwidget(QMap<QString, QString>);
    void createWidget(QString objectType, QString userName, int posX,int posY,int w,int h, QVariant data, QString imgOrPhoto, QFont font);
    void screenshot(QString str, bool toFile);
    //void printScreenWithParameters(QString);

public slots:
    void closeCardSecondWindow(QCloseEvent *);
    void deleteWidget(QString);
    void editWidget(void);
    void editWidgetImage(void);
    void raiseWidget(QString);
    void lowerWidget(QString);
    void receiveChangedParametersOfWidget(ParametersOfWidget);
    void receiveWidgetName(QString str);
    void consoleScreen(void);
    void receiveImageFromCamera(QImage);
    void editWidgetCamera();

private slots:
    void mousePressEvent(QMouseEvent *event);
    //void mouseMoveEvent(QMouseEvent *event);
    //void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent* event);
    void resizeCard();
    ///
    int openFile(void);
    int openFile(QString);
    int setWidgetParameters(QMultiMap<QString, ConsoleWidgetParameters>);
    int loadWidgets(QString);
    ///
    int saveWidgets(void);
    ///
    void deleteAllWidgets(void);
    ///
    void printScreen();
    void printPreviewScreen();

protected:
    void dragEnterEvent(QDragEnterEvent *);
    void dropEvent(QDropEvent  *);
signals:
    void sentParametersOfWidget(ParametersOfWidget);
    void ok(QMultiMap<QString, ConsoleWidgetParameters>);
};


#endif // CARDMAINWINDOW_H
