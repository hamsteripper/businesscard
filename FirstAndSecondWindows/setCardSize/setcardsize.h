#ifndef SETCARDSIZE_H
#define SETCARDSIZE_H

#include <QDialog>
#include <include/inputoutput.h>

namespace Ui {
class SetCardSize;
}

class SetCardSize : public QDialog
{
    Q_OBJECT
    double width;
    double height;
public:
    explicit SetCardSize(QWidget *parent = 0);
    ~SetCardSize();
    QSize getSize();
public slots:
    void setSize(double, double);
    void on_yes_clicked();
    void on_no_clicked();

    void on_wight_textChanged();
    void on_height_textChanged();
signals:
    signalNewSize(double, double);

private:
    Ui::SetCardSize *ui;
};

#endif // SETCARDSIZE_H
