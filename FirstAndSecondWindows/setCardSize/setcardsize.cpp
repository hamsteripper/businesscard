#include "setcardsize.h"
#include "ui_setcardsize.h"

SetCardSize::SetCardSize(QWidget *parent) : QDialog(parent), ui(new Ui::SetCardSize)
{
    ui->setupUi(this);

    ui->wight->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"), this));
    ui->height->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"), this));

}

SetCardSize::~SetCardSize()
{
    delete ui;
}

void SetCardSize::setSize(double w, double h){
    width = w*0.264583333333334;
    height = h*0.264583333333334;

    ui->wight->setText(QString::number(width));
    ui->height->setText(QString::number(height));
}

QSize SetCardSize::getSize(){
    QSize size;
    size.setWidth(width/0.264583333333334);
    size.setHeight(height/0.264583333333334);
    return size;
}

// Сигнал нажатия кнопки да
void SetCardSize::on_yes_clicked(){
    this->accept();
}
// Сигнал нажатия кнопки нет
void SetCardSize::on_no_clicked(){
    this->reject();
}

void SetCardSize::on_wight_textChanged(){
    width = ui->wight->text().toDouble();
}

void SetCardSize::on_height_textChanged(){
    height = ui->height->text().toDouble();
}
