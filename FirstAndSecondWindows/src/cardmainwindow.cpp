#include <FirstAndSecondWindows/include/cardmainwindow.h>

// Если пришли какие-то параметры
CardMainWindow::CardMainWindow(QMultiMap<QString, ConsoleWidgetParameters> mmap) : CardMainWindow(){


    this->mmap = new QMultiMap<QString, ConsoleWidgetParameters>(mmap);
    QMultiMap<QString, ConsoleWidgetParameters>::iterator it;

    // Если параметров нет
    if(!mmap.isEmpty()){
        // Открыть проект
        if(mmap.find("fileOpen") != mmap.end()){
            it = mmap.find("fileOpen");
            ConsoleWidgetParameters param = it.value();
            mmap.erase(it);
            if(openFile(param.fileXmlPath)){
                // Изменение параметров виджетов открытого проекта
                if(mmap.find("changeSettings") != mmap.end()){
                    it = mmap.find("changeSettings");
                    mmap.erase(it);
                    setWidgetParameters(mmap);
                }
            }else{
                cout << "file not found" << endl;
                exit(0);
            }
        }
    }

    // Перерисовка
    this->repaint();
    // Счётчик отрисовки изображения, печати и закрытия
    // (если запихать их в конструктор, то отрисовка будет после скрина и скрин будет белым)
    // КОСТЫЫЫЫЫЫЫЫЛЬ
    thread = new counterThread();
    thread->setTime(150);
    QObject::connect(thread, SIGNAL(countStop(void)), this, SLOT(consoleScreen(void)));
    thread->start(QThread::LowestPriority);

}

CardMainWindow::CardMainWindow(){
    setWindowTitle("Главная форма");
    resize(500,300);
    this->setAcceptDrops(true);
    this->move(0,30);

    // Список виджетов на форме
    qWLst = new QWidgetList();
    listMap = new QList<QMap<QString, QString>>();
    QPixmap openpixPrint(":/images/printer.png");
    QPixmap openpixPreview(":/images/preview.png");
    // Холст
    canvas = new myCanvas(this);
    // Меню
    mnuBar = new QMenuBar(this);
    pmnu   = new QMenu("&Меню");
    pmnu->addAction("&Сохранить", this, SLOT(saveWidgets()), Qt::CTRL + Qt::Key_S);
    pmnu->addAction("&Открыть", this, SLOT(openFile()), Qt::CTRL + Qt::Key_O);
    pmnu->addAction("&Печать", this, SLOT(printScreen()), Qt::CTRL + Qt::Key_P);
    pmnu->addSeparator();
    pmnu->addAction("&Очистить", this, SLOT(deleteAllWidgets()), Qt::CTRL + Qt::Key_N);
    pmnu->addAction("&Изменить размер", this, SLOT(resizeCard()), Qt::CTRL + Qt::Key_N);

    mnuBar->addMenu(pmnu);
    print = new QAction(QIcon(openpixPrint), "printer.png", this);
    printPreview = new QAction(QIcon(openpixPreview), "preview.png", this);
    mnuBar->addAction(print);
    mnuBar->addAction(printPreview);
    mnuBar->show();

    // Установка меню
    this->setMenuBar(mnuBar);
    // Установка центрального виджета для рисования
    setCentralWidget(canvas);

    //    QStatusBar *sb = this->statusBar();
    //    QHBoxLayout *layout = new QHBoxLayout;
    //    layout->addWidget(new QLineEdit(this));
    //    layout->addWidget(new QLineEdit(this));
    //    QWidget *container = new QWidget;
    //    container->setLayout(layout);
    //    sb->addWidget(container);

    //    lineW = new QLineEdit(this);
    //    lineW->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    //    lineW->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"), this));
    //    lineW->setText(QString::number(this->widthMM()));
    //    lineH = new QLineEdit(this);
    //    lineH->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    //    lineH->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"), this));
    //    lineH->setText(QString::number(this->heightMM()-this->statusBar()->heightMM()));

    //    statusBar()->addWidget(new QLabel("Ширина(мм)",this));
    //    statusBar()->addWidget(lineW);
    //    statusBar()->addWidget(new QLabel("Высота(мм)",this));
    //    statusBar()->addWidget(lineH);

    this->show();

    openCardSecondWindow();
    createWidgetEditDialog();
    createConnects();
}

void CardMainWindow::printScreen(){

    screenshot("",false);
    myPrinter *print = new myPrinter();
    print->setPrintImage(currentImage);
    print->print(true, false);

}

void CardMainWindow::printPreviewScreen(){
    screenshot("",false);

    myPrinter *print = new myPrinter();
    print->setPrintImage(currentImage);
    print->print(false, true);
}

void CardMainWindow::resizeCard(){
    // Изменение размера карты
    SetCardSize *setCardSize = new SetCardSize(this);
    setCardSize->setSize(this->size().width(), this->size().height());
    setCardSize->setVisible(true);
    setCardSize->setModal(true);
    setCardSize->setWindowModality(Qt::ApplicationModal);

    if (setCardSize->exec() == QDialog::Accepted)
    {
        this->resize(setCardSize->getSize().width(), setCardSize->getSize().height());
    }
    setCardSize->close();
    delete setCardSize;
}

// Сделать скриншот и
// 1: Послать на печать
// 2: Сохранить в файл
void CardMainWindow::consoleScreen(void){

    QMultiMap<QString, ConsoleWidgetParameters>::iterator it;


    // Если параметр print найден, то печать скриншота
    if(mmap->find("print") != mmap->end()){
        it = mmap->find("print");
        ConsoleWidgetParameters param = it.value();
        mmap->erase(it);
        // Проверка, если задан ключ preview
        bool preview = false;
        if(mmap->find("preview") != mmap->end()){
            mmap->erase(mmap->find("preview"));
            preview = true;
        }
        // Проверка принтера на существование
        bool printDialog = false;
        if(QPrinterInfo::printerInfo(param.print).isNull()){
            printDialog = true;
        }
        ///
        QString userPrinterParameters = "";
        ///

        if(!printDialog){ // Если заданный принтер найден
            screenshot("", false);
            myPrinter *print = new myPrinter();
            print->setPrinter(param.print);
            // Если есть конфигурация
            print->openPrinterParameters("defaultPrinterParameters.txt");
            print->setPrintImage(currentImage);
            print->print(printDialog, preview);
            //print->savePrinterParameters();
        }else if (printDialog){ // Если заданный принтер не найден
            screenshot("", false);
            myPrinter *print = new myPrinter();
            // Если есть конфигурация
            print->openPrinterParameters("defaultPrinterParameters.txt");
            print->setPrintImage(currentImage);
            print->print(printDialog, preview);
            //print->savePrinterParameters();
        }
    }

    // Если параметр screenshot найден, то сохранение скриншота
    if(mmap->find("screenshot") != mmap->end()){
        it = mmap->find("screenshot");
        ConsoleWidgetParameters param = it.value();
        mmap->erase(it);
        if(param.screenshot != ""){
            // Сохранение скриншота в файл
            screenshot(param.screenshot, true);
        }else{
            cout << "screenshot name not find" << endl;
        }
    }

    // Выход
    if(mmap->find("exit") != mmap->end()){
        exit(0);
    }
}

// Удаление виджета с указаным именем
void CardMainWindow::deleteWidget(QString name){
    widgetEdit->hide();
    // Удаление виджета из списка
    foreach (QWidget *wgt, *qWLst) {
        if( wgt->objectName() == name)
            qWLst->removeOne(wgt);
    }
    // Удаление виджета
    int a = 0;
    int b = name.indexOf('_');
    QString subStr = name.mid(a,b);
    if(subStr == "myLable"){
        findChild<myLable*>(name)->deleteLater();
    }else if(subStr == "myLineEdit"){
        findChild<myLineEdit*>(name)->deleteLater();
    }else if(subStr == "myTextEdit"){
        findChild<myTextEdit*>(name)->deleteLater();
    }
}
// Начать редактирование
void CardMainWindow::editWidget(){
    widgetEdit->show();
}
// ВЫбор картинки
void CardMainWindow::editWidgetImage(){
    bool find = false;
    QWidget *wd;
    // Поиск виджета в списке
    foreach (QWidget *wgt, *qWLst) {
        if( wgt->objectName() == currentNameOfEditWidget){
            wd = wgt;
            find = true;
            break;
        }
    }
    // Если виджет не найден
    if(!find) return;
    QString path = QFileDialog::getOpenFileName(this, "Add image", "", nullptr, nullptr);
    QImage image(path);
    if(!image.isNull()){
        QPixmap pix(image.size());
        pix.fill(Qt::transparent);
        QPainter p;
        p.begin(&pix);
        p.drawPixmap(0, 0, QPixmap::fromImage(image));
        p.end();
        dynamic_cast<myLable*>(wd)->setPixmap(pix);
        dynamic_cast<myLable*>(wd)->setImgOrPhoto("img");
    }
}

// Поднять виджет на верх
void CardMainWindow::raiseWidget(QString name){
    for(int i = 0; i < qWLst->size(); i++){
        if(qWLst->at(i)->objectName() == name){
            qWLst->at(i)->raise();
            QWidget *wgt = qWLst->at(i);
            qWLst->removeAt(i);
            qWLst->append(wgt);
        }
    }
}
// Опустить виджет вниз
void CardMainWindow::lowerWidget(QString name){
    for(int i = 0; i < qWLst->size(); i++){
        if(qWLst->at(i)->objectName() == name){
            qWLst->at(i)->lower();
            QWidget *wgt = qWLst->at(i);
            qWLst->removeAt(i);
            qWLst->insert(0, wgt);
        }
    }
}

// Обработка нажатий клавиш мыши
void CardMainWindow::mousePressEvent(QMouseEvent* event){
    if (event->buttons() & Qt::LeftButton){
        //cout << "CardMainWindow::mousePressEvent --- Qt::LeftButton" << endl;
    }else if (event->buttons() & Qt::RightButton){
        //cout << "CardMainWindow::mousePressEvent --- Qt::RightButton" << endl;
        m_dragStart = event->pos();
    }else if (event->buttons() & Qt::MiddleButton){
        // cout << "CardMainWindow::mousePressEvent --- Qt::MiddleButton" << endl;
    }
    this->update();
}
// Перерисовка изображения
//void CardMainWindow::paintEvent(QPaintEvent *event){

//}
// Сигналы и слоты
void CardMainWindow::createConnects(){
    // Закытие вторичного окна
    QObject::connect(secondWin, SIGNAL(closeCardMainWindow(QCloseEvent*)), this, SLOT(closeCardSecondWindow(QCloseEvent*)));

    // Получение новых параметров из виджета CardSecondWindow
    QObject::connect(widgetEdit, SIGNAL(sendChangedParametersOfWidget(ParametersOfWidget)), this, SLOT(receiveChangedParametersOfWidget(ParametersOfWidget)));

    QObject::connect(this, SIGNAL(sentParametersOfWidget(ParametersOfWidget)), widgetEdit, SLOT(receiveParametersOfWidget(ParametersOfWidget)));

    QObject::connect(print, SIGNAL(triggered(bool)), this, SLOT(printScreen()));

    QObject::connect(printPreview, SIGNAL(triggered(bool)), this, SLOT(printPreviewScreen()));

}
// Получение имени виджета при нажатии
void CardMainWindow::receiveWidgetName(QString name){
    currentNameOfEditWidget = name;
    bool find = false;
    //QWidget *wd;
    // Поиск виджета в списке
    foreach (QWidget *wgt, *qWLst) {
        if( wgt->objectName() == name){
            //wd = wgt;
            find = true;
            break;
        }
    }
    // Если виджет не найден
    if(!find) return;
    // Создание структуры
    ParametersOfWidget param;
    // Удаление виджета
    int a = 0;
    int b = name.indexOf('_');
    QString subStr = name.mid(a,b);
    if(subStr == "myLable"){
        param.font = findChild<myLable*>(name)->font();
        param.objectname = name;
        param.str = findChild<myLable*>(name)->text();
        param.userName = findChild<myLable*>(name)->getName();
        emit sentParametersOfWidget(param);
    }else if(subStr == "myLineEdit"){
        param.font = findChild<myLineEdit*>(name)->font();
        param.objectname = name;
        param.str = findChild<myLineEdit*>(name)->text();
        param.userName = findChild<myLineEdit*>(name)->getName();
        emit sentParametersOfWidget(param);
    }else if(subStr == "myTextEdit"){
        param.font = findChild<myTextEdit*>(name)->font();
        param.objectname = name;
        param.str = findChild<myTextEdit*>(name)->toPlainText();
        param.userName = findChild<myTextEdit*>(name)->getName();
        emit sentParametersOfWidget(param);
    }
}
// Новые параметры элемента
void CardMainWindow::receiveChangedParametersOfWidget(ParametersOfWidget param){

    bool find = false;
    QWidget *wd;
    // Поиск виджета в списке
    foreach (QWidget *wgt, *qWLst) {
        if( wgt->objectName() == currentNameOfEditWidget){
            wd = wgt;
            find = true;
            break;
        }
    }
    // Если виджет не найден
    if(!find) return;
    // Удаление виджета
    int a = 0;
    int b = currentNameOfEditWidget.indexOf('_');
    QString subStr = currentNameOfEditWidget.mid(a,b);
    if(subStr == "myLable"){
        dynamic_cast<myLable*>(wd)->setFont(param.font);
        dynamic_cast<myLable*>(wd)->setText(param.str);
        dynamic_cast<myLable*>(wd)->setName(param.userName);
        if(param.str != ""){
            dynamic_cast<myLable*>(wd)->setImgOrPhoto("text");
        }
    }else if(subStr == "myLineEdit"){
        dynamic_cast<myLineEdit*>(wd)->setFont(param.font);
        dynamic_cast<myLineEdit*>(wd)->setText(param.str);
        dynamic_cast<myLineEdit*>(wd)->setName(param.userName);
    }else if(subStr == "myTextEdit"){
        dynamic_cast<myTextEdit*>(wd)->setFont(param.font);
        dynamic_cast<myTextEdit*>(wd)->setText(param.str);
        dynamic_cast<myTextEdit*>(wd)->setName(param.userName);
    }

}
// Слот определяющий, что вторичное окно закрылось
void CardMainWindow::closeCardSecondWindow(QCloseEvent *_e){
    qDebug() << _e;
    this->close();
}
// Создание вторичного окна
QString CardMainWindow::openCardSecondWindow(){
    secondWin = new CardSecondWindow();
    if(secondWin){
        secondWin->setWindowTitle("Элементы для перетаскивания");
        secondWin->resize(500,300);
        secondWin->show();
        return "TRUE";
    }else
        return "FALSE";
}
// Создание вторичного окна
void CardMainWindow::createWidgetEditDialog(){
    widgetEdit = new WidgetEditDialog();
    if(widgetEdit){
        widgetEdit->setWindowTitle("Редактирование элемента");
        widgetEdit->resize(500,300);
    }
}
// Закрытие главного окна
void CardMainWindow::closeEvent(QCloseEvent *event){
    // Удаление вторичного окна
    if(secondWin)
        delete secondWin;
}
void CardMainWindow::dragEnterEvent(QDragEnterEvent *event){
    event->accept();
}

//void CardMainWindow::mouseMoveEvent(QMouseEvent *event){
//    //cout << "CardMainWindow::mouseMoveEvent" << endl;
//}

void CardMainWindow::dropEvent(QDropEvent *event){

    currentDropEvent = new QDropEvent(*event);
    QFont font;
    const QMimeData *mimeData = event->mimeData();
    // Если сброс текста
    if (mimeData->hasFormat("application/x-qabstractitemmodeldatalist")){
        QByteArray itemData = event->mimeData()->data("application/x-qabstractitemmodeldatalist");
        QDataStream stream(&itemData, QIODevice::ReadOnly);
        int row, col;

        QMap<int, QVariant> valueMap;
        stream >> row >> col >> valueMap;
        // Создание виджетов
        if(!valueMap.isEmpty()){
            if(valueMap.value(0).toString() == "lable"){
                createWidget("myLable", "", event->pos().x(), event->pos().y(), 50, 30, "myLable", "text", font);
            }else if(valueMap.value(0).toString() == "lineEdit"){
                createWidget("myLineEdit", "", event->pos().x(), event->pos().y(), 50, 30, "", "", font);
            }else if(valueMap.value(0).toString() == "textEdit"){
                createWidget("myTextEdit", "", event->pos().x(), event->pos().y(), 50, 30, "myTextEdit", "", font);
            }else if(valueMap.value(0).toString() == "image"){
                QString path = QFileDialog::getOpenFileName(this, "Add image", "", nullptr, nullptr);
                // Если нужно добавить изображение
                QImage image(path);
                if(!image.isNull()){

                    QPixmap pix(image.size());
                    pix.fill(Qt::transparent);
                    QPainter p;
                    p.begin(&pix);
                    p.drawPixmap(0, 0, QPixmap::fromImage(image));
                    p.end();

                    createWidget("myLable",
                                 "",
                                 event->pos().x(), event->pos().y(),
                                 200, 100,
                                 pix,
                                 "img", font);
                }else{
                    event->ignore();
                    return;
                }
            }else if(valueMap.value(0).toString() == "photo"){
                CameraWidget *cam = new CameraWidget();
                connect(cam,SIGNAL(sendImage(QImage)), this, SLOT(receiveImageFromCamera(QImage)));
                cam->exec();
                delete cam;
                delete currentDropEvent;
                currentDropEvent = nullptr;
            }
        }
        event->accept();
        return;
    }else{ // Если сброс картинки
        // Получение имени файла
        QString filePath = event->mimeData()->urls()[0].toLocalFile();
        // Создаём изображение
        QImage image(filePath);
        // А изображение ли это ?
        if(!image.isNull()){
            createWidget("myLable",
                         "",
                         event->pos().x(), event->pos().y(),
                         this->width(), this->height(),
                         transparentImage(image),
                         "img", font);
            event->accept();
            return;
        }else
            event->ignore();
    }
}

void CardMainWindow::receiveImageFromCamera(QImage img){

    QImage image(img);
    if(!image.isNull()){
        QFont font;
        QPixmap pix(image.size());
        pix.fill(Qt::transparent);
        QPainter p;
        p.begin(&pix);
        p.drawPixmap(0, 0, QPixmap::fromImage(image));
        p.end();
        // Фильтруем сигнал на редактирование и сигнал на создание нового виджета
        if(createWidgetFromCamera){
            createWidget("myLable",
                         "",
                         currentDropEvent->pos().x(), currentDropEvent->pos().y(),
                         200, 100,
                         pix,
                         "photo", font);
        }else{
            foreach (QWidget *wgt, *qWLst) {
                // Поиск объекта с заданным именем
                if(wgt->inherits("myLable")){
                    myLable *lable = dynamic_cast<myLable*>(wgt);
                    //qDebug() << lable->objectName();
                    if(lable->objectName() == currentNameOfEditWidget){
                        lable->setPixmap(pix);
                        lable->setImgOrPhoto("photo");
                    }
                }
            }
        }
    }else{
        currentDropEvent->ignore();
        createWidgetFromCamera = true;
        return;
    }
    createWidgetFromCamera = true;
}

void CardMainWindow::editWidgetCamera(){

    createWidgetFromCamera = false;
    CameraWidget *cam = new CameraWidget();
    connect(cam,SIGNAL(sendImage(QImage)), this, SLOT(receiveImageFromCamera(QImage)));
    cam->exec();
    cout << "modality" << endl;
    delete cam;

}

// Вычисление новой позиции виджета (ровняем по клеточкам)
QPoint CardMainWindow::positionCalculate(int x, int y){
    double w1D = x;
    double h1D = y;
    int x1 = w1D/step;
    int y1 = h1D/step;
    QPoint p;
    p.setX(x1*step);
    p.setY(y1*step);
    return p;
}

void CardMainWindow::keyPressEvent(QKeyEvent* event){
    cout << "CardMainWindow::keyPressEvent = " << event->key() << endl;
    switch(event->key()){
    case Qt::Key_Escape:{
        // Обработка нажатия Esc
        break;}
    case Qt::Key_Return:
        // Делаем скрин экрана
    case Qt::Key_Enter:{
        screenshot("test.jpg", true);
        break;
    }
        //Сохранение макета в xml файл
    case Qt::Key_S:{
        break;}
    case Qt::Key_O:{
        break;}
    }
}

void CardMainWindow::screenshot(QString str, bool toFile){

    // Не отрисовывать сетку
    canvas->screenStart();
    foreach (QWidget *wgt, *qWLst){
        wgt->setStyleSheet("background-color: rgba(255, 255, 255, 0);"
                           "border: none");
    }

    this->repaint();
    QScreen *QSCREEN = QGuiApplication::primaryScreen();
    QPixmap qpix = QSCREEN->grabWindow(canvas->winId());
    if(toFile)
        qpix.save(str);
    else
        currentImage = qpix;

    foreach (QWidget *wgt, *qWLst){
        wgt->setStyleSheet("color: rgb(0, 0, 0); "
                           "background-color: rgba(255, 255, 255, 90);"
                           "border: 1px dotted blue");
    }
    // Сетку можно отрисовывать
    canvas->screenFinish();

}

// Добавление элемента на форму
int CardMainWindow::addwidget(QMap<QString, QString> map){
    // Параметры объекта
    int posX = map.find("posX").value().toInt();
    int posY = map.find("posY").value().toInt();
    int w = map.find("width").value().toInt();
    int h = map.find("height").value().toInt();
    QString text = "";
    QString userName = "";
    //QString strFont = "";
//    if()
        QString strFont = map.find("font").value();
    QFont font;
    qDebug() << font.fromString(strFont);
    if(map.find("text") != map.end())
        text = map.find("text").value();


    // Изменения параметров главного окна как в исходных сохранённых в xml
    if(map.find("class").value() == "CardMainWindow")
        this->resize(w, h);

    if(map.find("userName") != map.end())
        userName = map.find("userName").value();

    // Если это ярлык (myLable)
    if(map.find("class").value() == "myLable"){
        QString imgOrPhoto = map.find("imgOrPhoto").value();
        // Установка картинки
        if(map.find("imageName") != map.end()){
            QPixmap pixmap(map.find("imageName").value());
            if(!pixmap.isNull()){
                createWidget("myLable", userName, posX, posY, w, h, pixmap, imgOrPhoto, font); // Картинка есть
            }else
                createWidget("myLable", userName, posX, posY, w, h, "Image not found", imgOrPhoto, font); // Картинки нет
        }else
            createWidget("myLable", userName, posX, posY, w, h, text, imgOrPhoto, font);
    }

    // Если это редактируемая строка (myLineEdit)
    if(map.find("class").value() == "myLineEdit"){
        createWidget("myLineEdit", userName, posX, posY, w, h, text, "", font);
    }

    // Если это редактируемая строка (myLineEdit)
    if(map.find("class").value() == "myTextEdit"){
        createWidget("myTextEdit", userName, posX, posY, w, h, text, "", font);
    }

    return 0;
}

// Метод инициализации и применения параметров к виджетам
void CardMainWindow::createWidget(QString objectType, QString userName,int posX,int posY,int w,int h, QVariant data, QString imgOrPhoto, QFont font){

    if(objectType == "myLineEdit"){
        myLineEdit *object = new myLineEdit(canvas);
        object->setFont(font);
        object->setText(data.value<QString>());
        QPoint p = positionCalculate(posX, posY);
        object->setGeometry(p.x(), p.y(), w, h);
        object->setObjectName("myLineEdit_" + QString::number(currentWidgetNumber));
        if(userName != "")
            object->setName(userName);
        else
            object->setName("myLineEdit_" + QString::number(currentWidgetNumber));
        qWLst->append(object);
        object->show();
        QObject::connect(object, SIGNAL(deleteObject(QString)), this, SLOT(deleteWidget(QString)));
        QObject::connect(object, SIGNAL(raiseObject(QString)), this, SLOT(raiseWidget(QString)));
        QObject::connect(object, SIGNAL(lowerObject(QString)), this, SLOT(lowerWidget(QString)));
        QObject::connect(object, SIGNAL(editObject(void)), this, SLOT(editWidget(void)));
        // Получение имени виджета
        QObject::connect(object, SIGNAL(sendName(QString)), this, SLOT(receiveWidgetName(QString)));
    }else if(objectType == "myLable"){
        myLable *object = new myLable(canvas);
        object->setFont(font);
        if(data.type() == QVariant::String){
            object->setText(data.value<QString>());
        }else if(data.type() == QVariant::Pixmap){
            object->setPixmap(data.value<QPixmap>());
        }
        object->setImgOrPhoto(imgOrPhoto);
        QPoint p = positionCalculate(posX, posY);
        object->setGeometry(p.x(), p.y(), w, h);
        object->setObjectName("myLable_" + QString::number(currentWidgetNumber));
        if(userName != "")
            object->setName(userName);
        else
            object->setName("myLineEdit_" + QString::number(currentWidgetNumber));
        qWLst->append(object);
        object->show();
        QObject::connect(object, SIGNAL(deleteObject(QString)), this, SLOT(deleteWidget(QString)));
        QObject::connect(object, SIGNAL(raiseObject(QString)), this, SLOT(raiseWidget(QString)));
        QObject::connect(object, SIGNAL(lowerObject(QString)), this, SLOT(lowerWidget(QString)));
        QObject::connect(object, SIGNAL(editObject(void)), this, SLOT(editWidget(void)));
        QObject::connect(object, SIGNAL(editObjectCamera(void)), this, SLOT(editWidgetCamera(void)));
        QObject::connect(object, SIGNAL(editObjectImg(void)), this, SLOT(editWidgetImage(void)));
        // Получение имени виджета
        QObject::connect(object, SIGNAL(sendName(QString)), this, SLOT(receiveWidgetName(QString)));
    }else if(objectType == "myTextEdit"){
        myTextEdit *object = new myTextEdit(canvas);
        object->setFont(font);
        object->setText(data.value<QString>());
        QPoint p = positionCalculate(posX, posY);
        object->setGeometry(p.x(), p.y(), w, h);
        object->setObjectName("myTextEdit_" + QString::number(currentWidgetNumber));
        if(userName != "")
            object->setName(userName);
        else
            object->setName("myLineEdit_" + QString::number(currentWidgetNumber));
        qWLst->append(object);
        object->show();
        QObject::connect(object, SIGNAL(deleteObject(QString)), this, SLOT(deleteWidget(QString)));
        QObject::connect(object, SIGNAL(raiseObject(QString)), this, SLOT(raiseWidget(QString)));
        QObject::connect(object, SIGNAL(lowerObject(QString)), this, SLOT(lowerWidget(QString)));
        QObject::connect(object, SIGNAL(editObject(void)), this, SLOT(editWidget(void)));
        // Получение имени виджета
        QObject::connect(object, SIGNAL(sendName(QString)), this, SLOT(receiveWidgetName(QString)));
    }
    currentWidgetNumber++;
}

// Удаление всех виджетов существующих в QWidgetList (т.е. всех на холсте)
void CardMainWindow::deleteAllWidgets(){
    widgetEdit->hide();
    foreach (QWidget *wgt, *qWLst) {
        // Проверка на тип и преобразование
        if(wgt->inherits("myLable")){
            delete dynamic_cast<myLable*>(wgt);
        }else if(wgt->inherits("myLineEdit")){
            delete dynamic_cast<myLineEdit*>(wgt);
        }else if(wgt->inherits("myTextEdit")){
            delete dynamic_cast<myTextEdit*>(wgt);
        }
    }
    qWLst->clear();
}
// Сохранить виджеты в файл
int CardMainWindow::saveWidgets(){

    // Открытие файла для сохранения
    QString path = QFileDialog::getSaveFileName(this, "Save report", "", "(*.xml)", nullptr);

    // Подготавливаем папку, куда будем складывать файлы проекта
    QFileInfo fn(path);
    QString fileName = fn.fileName();
    int a = fileName.indexOf(".xml");
    fileName = fileName.mid(0,a);
    QDir::setCurrent(fn.path());
    qDebug() << fileName;
    if (!QDir(fileName).exists()){
        QDir().mkdir(fileName);
    }

    // Вызов класса чтения и записи в xml
    ReWriteXml rwXml;
    // Установка имени файла
    int status = rwXml.setFile(path, QIODevice::WriteOnly);
    switch(status){
    case 0:
        break;
    case 1:
        break;
    case -1:
        return -1;
    case -10:
        return -1;
    }
    // Очистка списка используемого для формирования xml
    listMap->clear();
    // Создание контейнра для сохранения параметров о главном окне
    QMap<QString, QString> mapss;
    mapss.insert("class","CardMainWindow");
    mapss.insert("posX",QString::number(this->pos().x()));
    mapss.insert("posY",QString::number(this->pos().y()));
    mapss.insert("width",QString::number(this->width()));
    mapss.insert("height",QString::number(this->height()));
    mapss.insert("font",this->font().toString());
    listMap->append(mapss);

    for(int i = 0; i < qWLst->size(); i++){
        // Забираем один виджет
        QWidget *wgt = qWLst->at(i);
        // Проверям его тип и записываем его параметры в map
        if(wgt->inherits("myLable")){
            QMap<QString, QString> mapss;
            mapss.insert("class","myLable");
            mapss.insert("posX",QString::number(dynamic_cast<myLable*>(wgt)->pos().x()));
            mapss.insert("posY",QString::number(dynamic_cast<myLable*>(wgt)->pos().y()));
            mapss.insert("width",QString::number(dynamic_cast<myLable*>(wgt)->width()));
            mapss.insert("height",QString::number(dynamic_cast<myLable*>(wgt)->height()));
            mapss.insert("userName", dynamic_cast<myLable*>(wgt)->getName());
            // Проверка на присутствие изображения, если оно есть,
            // то придумаваем ему имя и сохраняем его под новым именем в папке проекта
            if(dynamic_cast<myLable*>(wgt)->pixmap()){
                mapss.insert("imageName",fileName + "/" +"image" + QString::number(i+1) + ".png");
                dynamic_cast<myLable*>(wgt)->pixmap()->save(fileName + "/" +"image" + QString::number(i+1) + ".png", "png", 100);
            }
            // Проверка на присутствие текста
            if(dynamic_cast<myLable*>(wgt)->text() != ""){
                mapss.insert("text",dynamic_cast<myLable*>(wgt)->text());
            }
            //
            mapss.insert("imgOrPhoto",dynamic_cast<myLable*>(wgt)->getImgOrPhoto());
            mapss.insert("font",dynamic_cast<myLable*>(wgt)->font().toString());
            // Добавляем map в список
            listMap->append(mapss);
        }else if(wgt->inherits("myLineEdit")){
            QMap<QString, QString> mapss;
            mapss.insert("class","myLineEdit");
            mapss.insert("posX",QString::number(dynamic_cast<myLineEdit*>(wgt)->pos().x()));
            mapss.insert("posY",QString::number(dynamic_cast<myLineEdit*>(wgt)->pos().y()));
            mapss.insert("width",QString::number(dynamic_cast<myLineEdit*>(wgt)->width()));
            mapss.insert("height",QString::number(dynamic_cast<myLineEdit*>(wgt)->height()));
            mapss.insert("userName", dynamic_cast<myLineEdit*>(wgt)->getName());
            if(dynamic_cast<myLineEdit*>(wgt)->text() != "")
                mapss.insert("text",dynamic_cast<myLineEdit*>(wgt)->text());
            mapss.insert("font",dynamic_cast<myLineEdit*>(wgt)->font().toString());
            listMap->append(mapss);
        }else if(wgt->inherits("myTextEdit")){
            //qDebug() << "fonts" << dynamic_cast<myTextEdit*>(wgt)->font().toString();
            QMap<QString, QString> mapss;
            mapss.insert("class","myTextEdit");
            mapss.insert("posX",QString::number(dynamic_cast<myTextEdit*>(wgt)->pos().x()));
            mapss.insert("posY",QString::number(dynamic_cast<myTextEdit*>(wgt)->pos().y()));
            mapss.insert("width",QString::number(dynamic_cast<myTextEdit*>(wgt)->width()));
            mapss.insert("height",QString::number(dynamic_cast<myTextEdit*>(wgt)->height()));
            mapss.insert("userName", dynamic_cast<myTextEdit*>(wgt)->getName());
            if(dynamic_cast<myTextEdit*>(wgt)->toPlainText() != "")
                mapss.insert("text",dynamic_cast<myTextEdit*>(wgt)->toPlainText());
            mapss.insert("font",dynamic_cast<myTextEdit*>(wgt)->font().toString());
            listMap->append(mapss);
        }
    }
    // Запись структуры в xml
    rwXml.writeXml(listMap);
    return 0;
}
// Установка и проверка файла
int CardMainWindow::openFile(){

    // Открытие файла
    QString path = QFileDialog::getOpenFileName(0, "Select file", "", "*.xml");

    QFileInfo fn(path);
    //    QString fileName = fn.fileName();
    //    int a = fileName.indexOf(".xml");
    //    fileName = fileName.mid(0,a);
    QString filePath = fn.path();
    QDir::setCurrent(filePath);
    qDebug() << "currrent dir" << QDir::currentPath();

    if(loadWidgets(path) < 0)
        cout << "file not found" << endl;
    return 0;
}
// Установка и проверка файла (консоль)
int CardMainWindow::openFile(QString path){

    QFileInfo fn(path);
    //    QString fileName = fn.fileName();
    //    int a = fileName.indexOf(".xml");
    //    fileName = fileName.mid(0,a);
    QString filePath = fn.path();

    //qDebug() << filePath;
    //qDebug() << "C:\\Users\\donkey\\Documents\\QT\\BusinessCard\\build-BusinessCard-Desktop_Qt_5_8_0_MinGW_32bit-Release\\release";

    QDir::setCurrent(filePath);
    qDebug() << "currrent dir" << QDir::currentPath();

    if(loadWidgets(path) < 0){
        return 0;
    }else
        return 1;
}
// Загрузка проекта
int CardMainWindow::loadWidgets(QString filePath){

    // Вызов класса чтения и записи в xml
    ReWriteXml rwXml;
    // Установка имени файла
    int status = rwXml.setFile(filePath, QIODevice::ReadOnly);
    // Проверка существования файла
    switch(status){
    case 0: break;
    case 1: break;
    case -1: return -1;
    case -10: return -1;
    }
    // Очистка листа
    listMap->clear();
    // Заполнение заново
    *listMap = rwXml.readXml();
    // Удаление виджетов из списка виджетов
    if(qWLst != NULL){
        deleteAllWidgets();
    }
    // Если возвращаемая структура NULL то выход
    if(listMap == NULL) return -1;
    // Создание виджетов
    for(int i = 0; i < listMap->size(); i++){
        addwidget(listMap->at(i));
    }
    return 0;
}

// Изменение параметров виджетов открытого проекта
int CardMainWindow::setWidgetParameters(QMultiMap<QString, ConsoleWidgetParameters> mmap){

    QMultiMap<QString, ConsoleWidgetParameters>::iterator it;

    // Поиск новых параметров для виджетов
    while(true){
        bool nameNotFind = true;
        // Поиск имени "widget" в получнных параметрах
        if(mmap.find("widget") == mmap.end())
            break;
        ConsoleWidgetParameters param;
        it = mmap.find("widget");
        param = it.value();
        mmap.erase(it);

        // Поиск объекта с заданным именем и его изменение
        foreach (QWidget *wgt, *qWLst) {
            // Если найден виджет myLable
            if(wgt->inherits("myLable")){
                myLable *lable = dynamic_cast<myLable*>(wgt);
                // Если виджет найден
                if(lable->getName() == param.userName){
                    if(param.imagePath != ""){ // Если у нас есть параметр изображение
                        // Пробуем открыть и установить изображение
                        QImage image(param.imagePath);
                        if(!image.isNull()){
                            QPixmap pix(image.size());
                            pix.fill(Qt::transparent);
                            QPainter p;
                            p.begin(&pix);
                            p.drawPixmap(0, 0, QPixmap::fromImage(image));
                            p.end();
                            lable->setPixmap(pix);
                        }else{
                            lable->setText("IMAGE NOT FOUND !!!");
                        }
                    }else if(param.text != ""){ // Если у нас есть параметр текст
                        lable->setText(param.text);
                    }else{
                        lable->setText("TEXT OR IMAGE NOT SPECIFIED !!!");
                    }
                    nameNotFind = false;
                }
            }
            // Если найден виджет myLineEdit
            if(wgt->inherits("myLineEdit")){
                myLineEdit *lineEdit = dynamic_cast<myLineEdit*>(wgt);
                if(lineEdit->getName() == param.userName){
                    if(param.text != ""){
                        lineEdit->setText(param.text);
                    }else{
                        lineEdit->setText("TEXT NOT FOUND !!!");
                    }
                    nameNotFind = false;
                }

            }
            // Если найден виджет myTextEdit
            if(wgt->inherits("myTextEdit")){
                myTextEdit *textEdit = dynamic_cast<myTextEdit*>(wgt);
                if(textEdit->getName() == param.userName){
                    if(param.text != ""){
                        textEdit->setText(param.text);
                    }else{
                        textEdit->setText("TEXT NOT FOUND !!!");
                    }
                    nameNotFind = false;
                }
            }
        }
        // Вывод ошибки, если не нашёл виджет по имени заданным пользователем
        if(nameNotFind)
            cout << "Name if not find: " + param.userName.toStdString() << endl;
    }



    // Сделать фото (Поиск ключа вызова камеры)
    it = mmap.find("camera");
    if(it != mmap.end()){
        mmap.erase(it);
        // Поиск ключа, в котором содержится имя виджета, для которого происходит вызов камеры
        it = mmap.find("camW");
        if(it != mmap.end()){
            ConsoleWidgetParameters param;
            param = it.value();
            // Если параметр не имеет значения
            if(param.camW == ""){
                cout << "key \"camW\" is empty " << endl;
                return -1;
            }
            bool widgetNotFound = true;
            // Поиск виджета по имени (только для myLable)
            foreach (QWidget *wgt, *qWLst){
                if(wgt->inherits("myLable")){
                    myLable *lable = dynamic_cast<myLable*>(wgt);
                    if(lable->getName() == param.camW){
                        currentNameOfEditWidget = lable->objectName();
                        createWidgetFromCamera = false;
                        CameraWidget *cam = new CameraWidget();
                        connect(cam,SIGNAL(sendImage(QImage)), this, SLOT(receiveImageFromCamera(QImage)));
                        cam->exec();
                        delete cam;
                        widgetNotFound = false;
                    }

                }
            }
            // Вывод ошибки, если не нашёл виджет по имени заданным пользователем
            if(widgetNotFound)
                cout << "Name if not find: " + param.userName.toStdString() << endl;
        }else{
            cout << "key \"camW\" not found" << endl;
        }


    }

    return 0;

}
