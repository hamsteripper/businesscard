#include "FirstAndSecondWindows/include/mycanvas.h"

myCanvas::myCanvas(){


}

myCanvas::myCanvas(QWidget *parent):QWidget(parent){

}
// Начало скриншота
void myCanvas::screenStart(){

    saveToImage = true;
    this->repaint();

}
// Сохранени скриншота
void myCanvas::screenFinish(){

    saveToImage = false;
    this->repaint();
}
// Отрисовка сетки
void myCanvas::paintEvent(QPaintEvent *event){

    //int x = 1;
    if(!saveToImage){
        int i = 0;
        double penSize = 0.7;
        while(i <= height()){
            i = i + step;
            QPainter p(this); // Создаём новый объект рисовальщика
            p.setPen(QPen(Qt::black, penSize, Qt::DashLine)); // Настройки рисования
            p.drawLine(0,i, width(), i); // Рисование линии
        }
        i = 0;
        while(i <= width()){
            i = i + step;
            QPainter p(this); // Создаём новый объект рисовальщика
            p.setPen(QPen(Qt::black, penSize, Qt::DashLine)); // Настройки рисования
            p.drawLine(i, 0, i, height()); // Рисование линии
        }
    }

}
