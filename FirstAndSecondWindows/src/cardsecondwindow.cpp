#include "FirstAndSecondWindows/include/cardsecondwindow.h"
// Конструктор
CardSecondWindow::CardSecondWindow(){
    frameLayout = new QHBoxLayout(this);
    createListWidget();
    createLayout();
//    setWindowTitle("Элементы формы");
}

// Обработчик закрытие окна
void CardSecondWindow::closeEvent(QCloseEvent *_e){
    emit closeCardMainWindow(_e);
}

// Создание списка виджетов (иконок)
void CardSecondWindow::createListWidget(){
    // Список перетаскиваемых элементов
    itemListWidget = new QListWidget;

    // Добавление элемента
    QStringList *qstr = new QStringList;
    *qstr << "lable";
    *qstr << "lineEdit";
    *qstr << "textEdit";
    *qstr << "image";
    *qstr << "photo";

    // Название элемента
    foreach(QString str, *qstr){
        QListWidgetItem* listItem = new QListWidgetItem( str );
        // Закачиваем элемент
        listItem->setIcon( QPixmap( ":/images/" + str + ".png" ) );
        // Включаем возможность редактирования
        listItem->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled );
        itemListWidget->addItem( listItem );
        itemListWidget->setDragEnabled(true);
    }
}
//
void CardSecondWindow::createLayout(){
    frameLayout->addWidget(itemListWidget);
    this->setLayout(frameLayout);
}
