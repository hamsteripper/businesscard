#ifndef CONSOLETOMULTIMAP_H
#define CONSOLETOMULTIMAP_H

#include <QMultiMap>
#include "include/consolewidgetparameters.h"
#include <getopt.h>

QMultiMap<QString, ConsoleWidgetParameters> readArgs(int argc, char *argv[]);

#endif // CONSOLETOMULTIMAP_H
