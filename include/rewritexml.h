#ifndef REWRITEXML_H
#define REWRITEXML_H

#include <QString>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QIODevice>
#include <QFlags>
#include <QMap>
#include "include/inputoutput.h"
#include <QDir>
#include <QFileInfo>

class ReWriteXml{
private:
    QFile *file;
    QIODevice::OpenModeFlag flag;
public:
    ReWriteXml();
    ~ReWriteXml();

public:
    QList<QMap<QString, QString>> readXml();
    int writeXml(QList<QMap<QString, QString>>*);
    int setFile(QString, QIODevice::OpenModeFlag);
};


#endif // REWRITEXML_H
