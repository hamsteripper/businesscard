#ifndef COUNTERTHREAD_H
#define COUNTERTHREAD_H

// Это счётчик.
// После истечения времени посылает сигнал countStop

#include <QThread>
#include <QTime>
#include <QObject>
#include <windows.h>

class counterThread : public QThread{
    Q_OBJECT
public:
  counterThread();

  void setTime(int mSec);
  void run();
  void stop();

signals:
  void countStop();

private:
  int mSec = 150;

};

#endif // COUNTERTHREAD_H
