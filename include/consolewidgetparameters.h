#ifndef CONSOLEWIDGETPARAMETERS_H
#define CONSOLEWIDGETPARAMETERS_H

#include <QString>

struct ConsoleWidgetParameters
{
    QString fileXmlPath = "";
    bool changeSettings = false;

    QString userName = "";
    QString text = "";
    QString imagePath = "";
    QString screenshot = "";
    QString print = "";
    QString camera = "";
    QString camW = "";
    bool preview = false;
    bool exit = false;

    void clear(void){
        fileXmlPath = "";
        changeSettings = false;
        userName = "";
        text = "";
        imagePath = "";
        screenshot = "";
        print = "";
        preview = false;
        camera = "";
        camW = "";
        exit = false;
    }
};

#endif // CONSOLEWIDGETPARAMETERS_H
