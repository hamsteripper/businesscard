#ifndef PARAMETERSOFWIDGET_H
#define PARAMETERSOFWIDGET_H

#include <QString>
#include <QFont>

struct ParametersOfWidget{

    QString userName;
    QString objectname;
    QString str;
    QFont font;

};


#endif // PARAMETERSOFWIDGET_H
