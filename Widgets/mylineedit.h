#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H

#include <QLineEdit>
#include <QObject>
#include <QMouseEvent>
#include "include/inputoutput.h"
#include "Widgets/widgetCM.h"

class myLineEdit : public QLineEdit{
    Q_OBJECT
private:
    QPoint offset;
    bool b_move;
    int step = 13;
    int button = 10000;
    QPoint difference;
    QMenu *m_menu;
    QAction* deleteAction;
    QAction* upAction;
    QAction* downAction;
    QAction* editAction;
    Qt::CursorShape cursoreStatus;
    QString username;
public:
    myLineEdit(QWidget *parent);
    void setName(QString);
    QString getName(void);
private:
    template <class T> friend void mousePressEventOut(QMouseEvent *event, T&);
    template <class T> friend void mouseMoveEventOut(QMouseEvent *event, T&);
    template <class T> friend void mouseReleaseEventOut(QMouseEvent *event, T&);
    void createConnects(void);
private slots:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void paintEvent(QPaintEvent *event);
    void menuClicked(QString);
    void contextMenuEvent( QContextMenuEvent* e );
    void change(void);
signals:
    void deleteObject(QString);
    void raiseObject(QString);
    void lowerObject(QString);
    void editObject(void);
    void sendName(QString);
};

#endif // MYLINEEDIT_H
