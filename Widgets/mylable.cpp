#include "Widgets/mylable.h"

myLable::myLable(QWidget *parent):QLabel(parent){
    setMouseTracking(true);
    setMinimumSize(QSize(13,13));

    setScaledContents(true);
    setStyleSheet("color: rgb(0, 0, 0); "
                  "background-color: rgba(255, 255, 255, 90);"
                  "border: 1px dotted blue");

    m_menu = new QMenu( this );
    deleteAction = m_menu->addAction( "Удалить" );
    editAction = m_menu->addAction( "Редактировать текст" );
    editImage = m_menu->addAction( "Вставить изображение" );
    editCamera = m_menu->addAction( "Вставить фото" );
    upAction = m_menu->addAction( "Поднять наверх" );
    downAction = m_menu->addAction( "Опустить вниз" );

    createConnects();
}
// Инициализация слотов
void myLable::createConnects(void){

    QSignalMapper* psigMapper = new QSignalMapper(this);
    QObject::connect(psigMapper, SIGNAL(mapped(const QString&)), this, SLOT(menuClicked(const QString&)));

    QObject::connect(deleteAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(deleteAction, "DELETE");

    QObject::connect(editAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(editAction, "EDIT");

    QObject::connect(editImage, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(editImage, "IMG");

    QObject::connect(editCamera, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(editCamera, "PHOTO");

    QObject::connect(upAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(upAction, "UP");

    QObject::connect(downAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(downAction, "DOWN");

}

// Обработчик нажатия на меню
void myLable::menuClicked(QString str){
    if(str == "DELETE")
        emit deleteObject(this->objectName());
    if(str == "EDIT")
        emit editObject();
    if(str == "IMG")
        emit editObjectImg();
    if(str == "PHOTO")
        emit editObjectCamera();
    if(str == "UP")
        emit raiseObject(this->objectName());
    if(str == "DOWN")
        emit lowerObject(this->objectName());
}

// Нажатие мыши
void myLable::mousePressEvent(QMouseEvent *event){
    change();
    mousePressEventOut(event, *this);
}
// Перерсовка виджета
void myLable::paintEvent(QPaintEvent *event){
    QLabel::paintEvent(event);
}
// Движение мыши
void myLable::mouseMoveEvent(QMouseEvent *event){
    mouseMoveEventOut(event, *this);
    setCursor(cursoreStatus);
}
// Отжатие мыши
void myLable::mouseReleaseEvent(QMouseEvent *event){
    mouseReleaseEventOut(event, *this);
}
// Вызов контекстного меню
void myLable::contextMenuEvent( QContextMenuEvent* e ) {
    if( m_menu ) {
        m_menu->exec( e->globalPos() );
    }
}
// Изменение текста или нажатие на виджет
void myLable::change(void){
    emit sendName(this->objectName());
}

//void myLable::focusInEvent(QFocusEvent *ev){
////    cout << "myLable::focusInEvent" << endl;
//}

//void myLable::focusOutEvent(QFocusEvent *ev){
////    cout << "myLable::focusOutEvent" << endl;
//}
// Задать пользовательское имя виджета
void myLable::setName(QString newName){
    username = newName;
}
// Получить пользовательское имя виджета
QString myLable::getName(void){
    return username;
}
//
void myLable::setImgOrPhoto(QString str){
    if(str == "img"){
        img = true;
        photo = false;
        textStr = false;
    }else if(str == "photo"){
        img = false;
        photo = true;
        textStr = false;
    }else if(str == "text"){
        img = false;
        photo = false;
        textStr = true;
    }
}
//
QString myLable::getImgOrPhoto(void){
    if(img)
        return "img";
    else if(photo)
        return "photo";
    else if(textStr)
        return "text";
}
//
void myLable::mouseDoubleClickEvent(QMouseEvent *event){
    change();
    if(img){
        emit editObjectImg();
    }else if(photo){
        emit editObjectCamera();
    }else if(textStr){
        emit editObject();
    }
}
