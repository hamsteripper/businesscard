#include "Widgets/mylineedit.h"

myLineEdit::myLineEdit(QWidget *parent):QLineEdit(parent){
    setStyleSheet("color: rgb(0, 0, 0); "
                  "background-color: rgba(255, 255, 255, 90);"
                  "border: 1px dotted blue");

    m_menu = new QMenu( this );
    m_menu->addSeparator();
    deleteAction = m_menu->addAction( "Удалить" );
    editAction = m_menu->addAction( "Редактировать текст" );
    upAction = m_menu->addAction( "Поднять наверх" );
    downAction = m_menu->addAction( "Опустить вниз" );

    createConnects();
}
// Инициализация слотов
void myLineEdit::createConnects(void){

    QSignalMapper* psigMapper = new QSignalMapper(this);
    QObject::connect(psigMapper, SIGNAL(mapped(const QString&)), this, SLOT(menuClicked(const QString&)));

    QObject::connect(deleteAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(deleteAction, "DELETE");

    QObject::connect(editAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(editAction, "EDIT");

    QObject::connect(upAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(upAction, "UP");

    QObject::connect(downAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(downAction, "DOWN");

    QObject::connect(this, SIGNAL(textChanged(QString)), this, SLOT(change(void)));
}

// Обработчик нажатия на меню
void myLineEdit::menuClicked(QString str){
    if(str == "DELETE")
        emit deleteObject(this->objectName());
    if(str == "EDIT")
        emit editObject();
    if(str == "UP")
        emit raiseObject(this->objectName());
    if(str == "DOWN")
        emit lowerObject(this->objectName());
}

void myLineEdit::mousePressEvent(QMouseEvent *event){
    change();
    mousePressEventOut(event, *this);
}
// Движение мыши
void myLineEdit::mouseMoveEvent(QMouseEvent *event){
    mouseMoveEventOut(event, *this);
    setCursor(cursoreStatus);
}
void myLineEdit::mouseReleaseEvent(QMouseEvent *event){
    mouseReleaseEventOut(event, *this);
}
void myLineEdit::enterEvent(QEvent *event){
    event->accept();
}
void myLineEdit::paintEvent(QPaintEvent *event){
    QLineEdit::paintEvent(event);
}
// Вызов контекстного меню
void myLineEdit::contextMenuEvent( QContextMenuEvent* e ) {
    if( m_menu ) {
        m_menu->exec( e->globalPos() );
    }
}
// Изменение текста или нажатие на виджет
void myLineEdit::change(void){
    emit sendName(this->objectName());
}
// Задать пользовательское имя виджета
void myLineEdit::setName(QString newName){
    username = newName;
}
// Получить пользовательское имя виджета
QString myLineEdit::getName(void){
    return username;
}
