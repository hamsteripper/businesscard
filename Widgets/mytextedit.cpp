#include "Widgets/mytextedit.h"

myTextEdit::myTextEdit(QWidget *parent):QTextEdit(parent){

    setMouseTracking(true);

    setStyleSheet("color: rgb(0, 0, 0); "
                  "background-color: rgba(255, 255, 255, 90);"
                  "border: 1px dotted blue");

    m_menu = createStandardContextMenu();
    m_menu->addSeparator();
    deleteAction = m_menu->addAction( "Удалить" );
    editAction = m_menu->addAction( "Редактировать текст" );
    upAction = m_menu->addAction( "Поднять наверх" );
    downAction = m_menu->addAction( "Опустить вниз" );

    createConnects();
}
// Инициализация слотов
void myTextEdit::createConnects(void){

    QSignalMapper* psigMapper = new QSignalMapper(this);
    QObject::connect(psigMapper, SIGNAL(mapped(const QString&)), this, SLOT(menuClicked(const QString&)));

    QObject::connect(deleteAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(deleteAction, "DELETE");

    QObject::connect(editAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(editAction, "EDIT");

    QObject::connect(upAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(upAction, "UP");

    QObject::connect(downAction, SIGNAL(triggered(bool)), psigMapper, SLOT(map()));
    psigMapper->setMapping(downAction, "DOWN");

}
// Обработчик нажатия на меню
void myTextEdit::menuClicked(QString str){
    if(str == "DELETE")
        emit deleteObject(this->objectName());
    if(str == "EDIT")
        emit editObject();
    if(str == "UP")
        emit raiseObject(this->objectName());
    if(str == "DOWN")
        emit lowerObject(this->objectName());
}
// Нажатие мыши
void myTextEdit::mousePressEvent(QMouseEvent *event){
    change();
    mousePressEventOut(event, *this);
}
// Движение мыши
void myTextEdit::mouseMoveEvent(QMouseEvent *event){
    mouseMoveEventOut(event, *this);
    viewport()->setCursor(cursoreStatus);
}
void myTextEdit::mouseReleaseEvent(QMouseEvent *event){
    mouseReleaseEventOut(event, *this);
}
void myTextEdit::enterEvent(QEvent *event){
    event->accept();
}
void myTextEdit::paintEvent(QPaintEvent *event){
    QTextEdit::paintEvent(event);
}
// Вызов контекстного меню
void myTextEdit::contextMenuEvent( QContextMenuEvent* e ) {
    if( m_menu ) {
        m_menu->exec( e->globalPos() );
    }
}
// Изменение текста или нажатие на виджет
void myTextEdit::change(void){
    emit sendName(this->objectName());
}
// Задать пользовательское имя виджета
void myTextEdit::setName(QString newName){
    username = newName;
}
// Получить пользовательское имя виджета
QString myTextEdit::getName(void){
    return username;
}
