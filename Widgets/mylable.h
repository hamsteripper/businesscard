#ifndef MYLABLE_H
#define MYLABLE_H
#include <QLabel>
#include <QObject>
#include <QMouseEvent>
#include <QApplication>
#include <QMenu>
#include <QSignalMapper>
#include "FirstAndSecondWindows/include/cardmainwindow.h"
#include "include/inputoutput.h"
#include "Widgets/widgetCM.h"

class myLable : public QLabel{
    Q_OBJECT
private:
    QPoint offset;
    bool b_move;
    int step = 13;
    int button = 10000;
    QPoint difference;
    QMenu *m_menu;
    QAction* deleteAction;
    QAction* upAction;
    QAction* downAction;
    QAction* editAction;
    QAction* editImage;
    QAction* editCamera;
    Qt::CursorShape cursoreStatus;
    QString username;
    //*//
    bool img = true;
    bool photo = false;
    bool textStr = true;
    //*//
public:
    myLable(QWidget *parent);
    void setName(QString);
    QString getName(void);
    void setImgOrPhoto(QString);
    QString getImgOrPhoto(void);
private:
    template <class T> friend void mousePressEventOut(QMouseEvent *event, T&);
    template <class T> friend void mouseMoveEventOut(QMouseEvent *event, T&);
    template <class T> friend void mouseReleaseEventOut(QMouseEvent *event, T&);
    void createConnects(void);
private slots:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *);
    void contextMenuEvent( QContextMenuEvent* e );
    void menuClicked(QString);
    //void focusInEvent(QFocusEvent *ev);
    //void focusOutEvent(QFocusEvent *ev);
    void change(void);
    void mouseDoubleClickEvent(QMouseEvent *event);
signals:
    void deleteObject(QString);
    void raiseObject(QString);
    void lowerObject(QString);
    void editObject(void);
    void editObjectImg(void);
    void sendName(QString);
    void editObjectCamera(void);
};

#endif // MYLABLE_H
