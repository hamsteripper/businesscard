#ifndef CARCASSWIDGET_H
#define CARCASSWIDGET_H
#include <Widgets/mytextedit.h>
#include <Widgets/mylineedit.h>
#include <Widgets/mylable.h>
#include <typeinfo>

// Высчитываем попадает ли мышь в области границ фигуры
// (3 и 10 пикселей)
int insideSquare(QPoint pos, int w, int h);
// Подсчёт псевдоскаляра
int pseudoScalar(QPoint A, QPoint B, QPoint C);
// Вычисление координат ближайшей клетки
QPoint shiftCalculate(int w, int h, int step);
// Создание прозрачного изображения
QPixmap transparentImage(QImage &filePath);

// Подгон размера, относительно окна
template <class T>
void maxResize(T& myClass){

    int w = myClass.width();
    int h = myClass.height();
    int parentW = myClass.parentWidget()->width();
    int parentH = myClass.parentWidget()->height();
    QSize size = myClass.parentWidget()->size();

    // Ограничение размера
    if(w > parentW && h > parentH){
        myClass.resize(size);
        myClass.move(0,0);
    }else if(w > parentW){
        myClass.resize(parentW, h);
        myClass.move(0, myClass.mapToParent(QPoint(0,0)).y());
    }else if(h > parentH){
        myClass.resize(w, parentH);
        myClass.move(myClass.mapToParent(QPoint(0,0)).x(), 0);
    }
}

template <class T>
void mousePressEventOut(QMouseEvent *event, T& myClass){
    cout << "mousePressEvent " << myClass.objectName().toStdString() << endl;
    maxResize(myClass);
    myClass.offset = event->pos();
}

// Движение мыши
template <class T>
void mouseMoveEventOut(QMouseEvent *event, T& myClass){

    if(event->buttons() & Qt::LeftButton){
        // Высчитывание попадания мыши в рамку (border)
        QPoint *offset = &myClass.offset;
        int inArea = insideSquare(*offset, myClass.width(), myClass.height());
        // Величина сдвига
        myClass.difference = event->pos() - *offset;
        QPoint difference = myClass.difference;
        int x = myClass.mapToParent(difference).x();
        int y = myClass.mapToParent(difference).y();
        int w = myClass.width();
        int h = myClass.height();
        int parentW = myClass.parentWidget()->width();
        int parentH = myClass.parentWidget()->height();
        // case 0: Перемещение внутри главного окна
        // case 1,2,3,4: Изменение размера объекта
        switch(inArea){
        case 0: // Перемещение внутри главного окна

            if(x <=0 && y <= 0){ //левая+верхняя
                myClass.move(1, 1);
            }
            else if(x <= 0 && y + h >= parentH){ //левая+нижняя
                myClass.move(1, parentH - h - 1);
            }
            else if(x + w >= parentW && y <= 0){ //правая+верхняя
                myClass.move(parentW - w - 1, 1);
            }
            else if(x + w >= parentW && y + h >= parentH){ // правая + нижняя
                myClass.move(parentW - w - 1, parentH - h - 1);
            }
            else if(y <= 0 && x > 0){ //верхняя
                myClass.move(myClass.mapToParent(difference).x(), 1);
            }
            else if(x <= 0 && y > 0){ // левая
                myClass.move(1, myClass.mapToParent(difference).y());
            }
            else if(x + w >= parentW){ // правая
                myClass.move(parentW - w - 1, myClass.mapToParent(difference).y());
            }
            else if(y + h >= parentH){ // нижняя
                myClass.move(myClass.mapToParent(difference).x(), parentH - h - 1);
            }
            else{ // Если в пределах границы
                myClass.move(myClass.mapToParent(difference));
            }
            myClass.button = 1;
            break;
        case 1:
            // Объект двигается влево или вправо
            myClass.move(myClass.mapToParent(difference).x(), myClass.mapToParent(QPoint(0,0)).y());
            // Изменяем размер объекта
            myClass.resize(w  + (difference).x()*(-1), h);
            myClass.button = 99999;
            break;
        case 2:
            // Объект двигается вверх или вниз
            myClass.move(myClass.mapToParent(QPoint(0,0)).x(), myClass.mapToParent(difference).y());
            // Изменяем размер объекта
            myClass.resize(w, h + (difference).y()*(-1));
            myClass.button = 99999;
            break;
        case 3:
            // Объект не двигается
            myClass.move(myClass.mapToParent(QPoint(0,0)).x(), myClass.mapToParent(QPoint(0,0)).y());
            // Изменяем размер объекта
            myClass.resize(w + (difference).x(), h);
            // Текущая точка захвата сдвинулась на значения сдвига правого конца объекта
            offset->setX(offset->x() + (difference).x());
            myClass.button = 99999;
            break;
        case 4:
            // Объект не двигается
            myClass.move(myClass.mapToParent(QPoint(0,0)).x(), myClass.mapToParent(QPoint(0,0)).y());
            // Изменяем размер объекта
            myClass.resize(w, h   + (difference).y());
            // Текущая точка захвата сдвинулась на значения сдвига нижнего конца объекта
            offset->setY(offset->y() + (difference).y());
            myClass.button = 99999;
            break;
        case 5:
            // Объект двигается (влево или вправо) и (вверх или вниз)
            myClass.move(myClass.mapToParent(difference).x(), myClass.mapToParent(difference).y());
            // Изменяем размер объекта
            myClass.resize(w  + (difference).x()*(-1), h + (difference).y()*(-1));
            myClass.button = 99999;
            break;
        case 6:
            // Объект двигается (вверх или вниз)
            myClass.move(myClass.mapToParent(QPoint(0,0)).x(), myClass.mapToParent(difference).y());
            // Изменяем размер объекта
            myClass.resize(w + (difference).x(), h + (difference).y()*(-1));
            // Текущая точка захвата сдвинулась на значения сдвига
            offset->setX(offset->x() + (difference).x());
            myClass.button = 99999;
            break;
        case 7:
            // Объект не двигается
            myClass.move(myClass.mapToParent(QPoint(0,0)).x(), myClass.mapToParent(QPoint(0,0)).y());
            // Изменяем размер объекта
            myClass.resize(w + (difference).x(), h + (difference).y());
            // Текущая точка захвата сдвинулась на значения сдвига
            offset->setX(offset->x() + (difference).x());
            offset->setY(offset->y() + (difference).y());
            myClass.button = 99999;
            break;
        case 8:
            // Объект двигается (влево или вправо)
            myClass.move(myClass.mapToParent(difference).x(), myClass.mapToParent(QPoint(0,0)).y());
            // Изменяем размер объекта
            myClass.resize(w + (difference).x()*(-1), h   + (difference).y());
            // Текущая точка захвата сдвинулась на значения сдвига
            offset->setY(offset->y() + (difference).y());
            myClass.button = 99999;
            break;
        }
    }else{
        // Если мышь находить на рамке фигуры то изменить её вид
        int inArea = insideSquare(event->pos(), myClass.width(), myClass.height());
        switch(inArea){
        case 0:
            myClass.cursoreStatus = Qt::ArrowCursor;
            break;
        case 1:
            myClass.cursoreStatus = Qt::SizeHorCursor;
            break;
        case 2:
            myClass.cursoreStatus = Qt::SizeVerCursor;
            break;
        case 3:
            myClass.cursoreStatus = Qt::SizeHorCursor;
            break;
        case 4:
            myClass.cursoreStatus = Qt::SizeVerCursor;
            break;
        case 5:
            myClass.cursoreStatus = Qt::SizeFDiagCursor;
            break;
        case 6:
            myClass.cursoreStatus = Qt::SizeBDiagCursor;
            break;
        case 7:
            myClass.cursoreStatus = Qt::SizeFDiagCursor;
            break;
        case 8:
            myClass.cursoreStatus = Qt::SizeBDiagCursor;
            break;
        }
    }
}

// Событие отпускания клавиши мыши
template <class T>
void mouseReleaseEventOut(QMouseEvent *event, T& myClass){
    maxResize(myClass);
    if(myClass.button == 1){
        QPoint difference = myClass.difference;
        int x = myClass.mapToParent(difference).x();
        int y = myClass.mapToParent(difference).y();
        int w = myClass.width();
        int h = myClass.height();
        int parentW = myClass.parentWidget()->width();
        int parentH = myClass.parentWidget()->height();

        // Если элемент был выпущен вне зоны главного окна
        if(x <=0 && y <= 0){ // левая + верхняя
            x = 1;
            y = 1;
        } else if(x <= 0 && y + h >= parentH){ // левая + нижняя
            x = 1;
            y = parentH - h;
        }else if(x + w >= parentW && y <= 0){ //правая + верхняя
            x = parentW - w;
            y = 1;
        }else if(x + w >= parentW && y + h >= parentH){ // правая + нижняя
            x = parentW - w;
            y = parentH - h;
        }
        else if(y <= 0 && x > 0){y = 1;} // верхняя
        else if(x <= 0 && y > 0){x = 1;} // левая
        else if(x + w >= parentW){x = parentW - w;} // правая
        else if(y + h >= parentH){y = parentH - h;} // нижняя

        myClass.move(shiftCalculate(x,y, myClass.step));
        myClass.button = 99999;
    }
}

#endif // CARCASSWIDGET_H
