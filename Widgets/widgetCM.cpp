#include <Widgets/widgetCM.h>

// Создание прозрачного изображения
QPixmap transparentImage(QImage &image){

    QPixmap transparent(image.size());
    transparent.fill(Qt::transparent);
    QPainter p;
    p.begin(&transparent);
    p.setCompositionMode(QPainter::CompositionMode_Source);
    p.drawPixmap(0, 0, QPixmap::fromImage(image));
    p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    p.fillRect(transparent.rect(), QColor(0, 0, 0, 150));
    p.end();

    return transparent;
}
// Вычисление псевдоскаляра
int pseudoScalar(QPoint A, QPoint B, QPoint C){
    return (B.y()-A.y())*(C.x()-A.x())-(B.x()-A.x())*(C.y()-A.y());
}
// Вычисление попадания виджета в квадрат сетки
int insideSquare(QPoint pos, int w, int h){

    // Левый нижний
    QPoint A(0,0);
    QPoint B(0,10);
    QPoint C(10,0);
    int N1 = pseudoScalar(A,B,pos);
    int N2 = pseudoScalar(B,C,pos);
    int N3 = pseudoScalar(C,A,pos);
    if ((N1>0 && N2>0  && N3>0) || (N1<0 && N2<0 && N3<0)){
        return 5;
    }
    // Правый верхний
    A.setX(w-10); A.setY(0);
    B.setX(w); B.setY(0);
    C.setX(w); C.setY(10);
    N1 = pseudoScalar(A,B,pos);
    N2 = pseudoScalar(B,C,pos);
    N3 = pseudoScalar(C,A,pos);
    if ((N1>0 && N2>0  && N3>0) || (N1<0 && N2<0 && N3<0)){
        return 6;
    }
    // Правый нижний
    A.setX(w-10); A.setY(h);
    B.setX(w); B.setY(h-10);
    C.setX(w); C.setY(h);
    N1 = pseudoScalar(A,B,pos);
    N2 = pseudoScalar(B,C,pos);
    N3 = pseudoScalar(C,A,pos);
    if ((N1>0 && N2>0  && N3>0) || (N1<0 && N2<0 && N3<0)){
        return 7;
    }
    // Левый нижний
    A.setX(0); A.setY(h);
    B.setX(0); B.setY(h-10);
    C.setX(10); C.setY(h);
    N1 = pseudoScalar(A,B,pos);
    N2 = pseudoScalar(B,C,pos);
    N3 = pseudoScalar(C,A,pos);
    if ((N1>0 && N2>0  && N3>0) || (N1<0 && N2<0 && N3<0)){
        return 8;
    }

    // Проверка на попадание в области слева, сверху, справа, снизу
    if (pos.x()>=0 && pos.y()>=0 && pos.x()<=3 && pos.y()<=h){
        return 1;
    }else if (pos.x()>=0 && pos.y()>=0 && pos.x()<=w && pos.y()<=3){
        return 2;
    }else if (pos.x()>=w-3 && pos.y()>=0 && pos.x()<=w && pos.y()<=h){
        return 3;
    }else if (pos.x()>=0 && pos.y()>=h-3 && pos.x()<=w && pos.y()<=h){
        return 4;
    }

    return 0;
}
// Подсчёт координат ближайшей клетки
QPoint shiftCalculate(int w, int h, int step){

    double w1D = w;
    double h1D = h;
    int x = w1D/step;
    int y = h1D/step;

    QPoint p;
    p.setX(x*step);
    p.setY(y*step);

    return p;
}
