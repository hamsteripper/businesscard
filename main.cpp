#include <QApplication>
#include <QDesktopWidget>
#include <FirstAndSecondWindows/include/cardmainwindow.h>


#include "include/consolewidgetparameters.h"
#include "include/consoletomultimap.h"



int main(int argc, char *argv[])
{
//    QStringList paths = QCoreApplication::libraryPaths();
//    paths.append(".");
//    paths.append("imageformats");
//    paths.append("platforms");
//    paths.append("sqldrivers");
//    QCoreApplication::setLibraryPaths(paths);


    QMultiMap<QString, ConsoleWidgetParameters> mmap = readArgs(argc, argv);

    QApplication a(argc, argv);
    CardMainWindow w(mmap);
    w.show();
    return a.exec();

    //--fileOpen="x1.xml" --changeSettings --userName="userName" --text="Емельянов Никита" --userName="description" --text="Инженер-Программист" --userName="company_lable" --image="0Kh5NbgNk7A.jpg"

}
