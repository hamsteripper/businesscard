#include "Printer/rewritePrinterSettings.h"


RewritePrinterSettings::RewritePrinterSettings(){
    file = new QFile();
}

int RewritePrinterSettings::setFile(QString path, QIODevice::OpenModeFlag flag){

    // Проверка существования файла
    if(!QFile::exists(path) && (flag == QIODevice::ReadOnly)){
        // Если нету, и установлено только чтение, то fatal error
        return -1;
    }else if (QFile::exists(path) && (flag == QIODevice::WriteOnly)){
        // Если есть, и установлена перезапись, то предупреждение
        file->setFileName(path);
        this->flag = flag;
        return 0;
    }else if (!QFile::exists(path) && (flag == QIODevice::WriteOnly)){
        // Если нету, и установлена перезапись, то спокойно открываем
        file->setFileName(path);
        this->flag = flag;
        return 1;
    }else if (QFile::exists(path) && (flag == QIODevice::ReadOnly)){
        // Если есть, и установлено только чтение, то спокойно открываем
        file->setFileName(path);
        this->flag = flag;
        return 1;
    }

    // Если выставленного флага нету
    return -10;

}

QList<QMap<QString, QString>> RewritePrinterSettings::readXml(){

    QList<QMap<QString, QString>> myStruct;

    if(file == NULL || file->fileName() == "")
        return myStruct;

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)){
        return myStruct;
    }else{
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(file);
        // Чтение первого элемента
        xmlReader.readNext();
        // До конца документа
        while(!xmlReader.atEnd()){
            // Проверка на элемент типа widget
            if(xmlReader.isStartElement() && xmlReader.name() == "Printer"){
                QMap<QString, QString> map;
                //Выполнять до тех пор, пока это  НЕ( Printer и конечный элемент )
                // (читаем всё, что между <Parameter></Parameter>)
                while(!(xmlReader.isEndElement() && xmlReader.name() == "Parameter")){
                    // Выискиваем имена (Copy, DoubleSidedPrinting, Orientation, PageSize, PrinterName)
                    // Если имя совпадает, и это стартовое значение, то записываем в map имя и значение
                    if(xmlReader.isStartElement() && xmlReader.name().toString() == "Copy"){
                        map.insert(xmlReader.name().toString(), xmlReader.readElementText());
                    }else if(xmlReader.isStartElement() && xmlReader.name().toString() == "DoubleSidedPrinting"){
                        map.insert(xmlReader.name().toString(), xmlReader.readElementText());
                    }else if(xmlReader.isStartElement() && xmlReader.name().toString() == "Orientation"){
                        map.insert(xmlReader.name().toString(), xmlReader.readElementText());
                    }else if(xmlReader.isStartElement() && xmlReader.name().toString() == "PageSize"){
                        map.insert(xmlReader.name().toString(), xmlReader.readElementText());
                    }else if(xmlReader.isStartElement() && xmlReader.name().toString() == "PrinterName"){
                        map.insert(xmlReader.name().toString(), xmlReader.readElementText());
                    }
                    xmlReader.readNext();
                }
                // Сохранение информации об одном виджете
                myStruct.append(map);
            }
            // Чтение дальше
            xmlReader.readNext();
        }
        // Закрытие файла
        file->close();
        return myStruct;
    }
}

int RewritePrinterSettings::writeXml(QList<QMap<QString, QString>> *myStruct){
    if(file == NULL || file->fileName() == "")
        return -1;
    file->open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(file);
    xmlWriter.setAutoFormatting(true);
    // Начало документа
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("Printer");
    // Запись параметров одного виджета
    for(int i = 0; i < myStruct->size(); i++){
        // Записываем новый виджет
        QMap<QString, QString> map = myStruct->at(i);
        xmlWriter.writeStartElement("Parameter");
        xmlWriter.writeAttribute("id", QString::number(i));
        // Запихиваем, ..., т.е. записываем параметры виджета.
        QMapIterator<QString, QString> iter(map);
        while(iter.hasNext()){
            iter.next();
            xmlWriter.writeStartElement(iter.key());
            xmlWriter.writeCharacters(iter.value());
            xmlWriter.writeEndElement();
        }
        // Закрываем виджет
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
    // Конец документа
    xmlWriter.writeEndDocument();
    file->close();
    return 0;
}

RewritePrinterSettings::~RewritePrinterSettings(){
    delete file;
}
