#ifndef MYPRINTER_H
#define MYPRINTER_H

#include <QPainter>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtGui>
#include <QtCore>
#include <QPageSize>
#include "Printer/rewritePrinterSettings.h"
#include <QMap>
#include <QList>
#include <include/inputoutput.h>
#include <QVector>
#include <QtPrintSupport/QPrintPreviewDialog>

class myPrinter : QObject{
    Q_OBJECT

    QPixmap image;
    QString printerName;
    QPrinter *printer;
    QPrinter *printerNew;
    QList<QMap<QString, QString>> *listMap = NULL;

public:
    myPrinter();

    int setPrinter(QString);

    void setPrintText(QString text);
    void setPrintTextFromFile(QString textPath);
    void setPrintImage(QPixmap image);
    void setPrintImageFromFile(QString imagePath);
    void print(bool, bool);
    int openPrinterParameters(QString str);
    int savePrinterParameters(QPrinter *aprinter);
    QString getPrinterName();

private:
    void oriantation(QPrinter *aprinter);
    QVector<QString> customPaper(QString);

private slots:
    void printWithPPD(QPrinter *aprinter);

};


#endif // MYPRINTER_H
