#include <Printer/myprinter.h>

myPrinter::myPrinter(){
    printer = new QPrinter();
    listMap = new QList<QMap<QString, QString>>();
}

void myPrinter::setPrintImage(QPixmap image){
    this->image = image;
}

int myPrinter::setPrinter(QString name){
    printer->setPrinterName(name);
    return 0;
}

QString myPrinter::getPrinterName(){
    return printer->printerName();
}

int myPrinter::savePrinterParameters(QPrinter *aprinter){
    // Вызов класса чтения и записи в xml
    RewritePrinterSettings rwXml;
    // Установка имени файла
    int status = rwXml.setFile("defaultPrinterParameters.txt", QIODevice::WriteOnly);
    switch(status){
    case 0:
        break;
    case 1:
        break;
    case -1:
        return -1;
    case -10:
        return -1;
    }
    // Очистка списка используемого для формирования xml
    listMap->clear();
    // Создание контейнра для сохранения параметров о принтере
    QMap<QString, QString> mapss;
    mapss.insert("PrinterName",aprinter->printerName());
    mapss.insert("PageSize",aprinter->pageLayout().pageSize().key());
    mapss.insert("Orientation",QString::number(aprinter->pageLayout().orientation()));
    mapss.insert("DoubleSidedPrinting",QString::number(aprinter->doubleSidedPrinting()));
    mapss.insert("Copy",QString::number(aprinter->copyCount()));
    listMap->append(mapss);
    // Запись структуры в xml
    rwXml.writeXml(listMap);

    return 0;
}

void myPrinter::print(bool dialog, bool preview){
    if(preview){
        oriantation(printer);
        QPrintPreviewDialog *pd = new QPrintPreviewDialog(printer);
        connect(pd,SIGNAL(paintRequested(QPrinter*)),this,SLOT(printWithPPD(QPrinter*)));
        pd->exec();
        // Сохранение параметров
    }else if(dialog){
        //openPrinterParameters("defaultPrinterParameters.txt");
        QPrintDialog *dlg = new QPrintDialog(printer,0);
        // Чтение файла параметров
        if(dlg->exec() == QDialog::Accepted) {
            oriantation(printer);
            // Сохранение параметров
            savePrinterParameters(printer);
            QImage img = image.toImage();
            QPainter painter(printer);
            painter.drawImage(QPoint(0,0),img);
            painter.end();
        }
        savePrinterParameters(printer);
    }else{
        //openPrinterParameters("defaultPrinterParameters.txt");
        oriantation(printer);
        QImage img = image.toImage();
        QPainter painter(printer);
        painter.drawImage(QPoint(0,0),img);
        painter.end();
        savePrinterParameters(printer);
    }
}

void myPrinter::oriantation(QPrinter *aprinter){

    if(aprinter->orientation() == QPrinter::Portrait){
        aprinter->setPaperSize(QSizeF(image.width()*0.264583333333334, image.height()*0.264583333333334), QPrinter::Millimeter);
    }else if(aprinter->orientation() == QPrinter::Landscape){
        aprinter->setPaperSize(QSizeF(image.height()*0.264583333333334, image.width()*0.264583333333334), QPrinter::Millimeter);
    }

}

void myPrinter::printWithPPD(QPrinter *aprinter){

    QImage img = image.toImage();
    oriantation(aprinter);
    QPainter painter(aprinter);
    painter.drawImage(QPoint(0,0),img);
    painter.end();

    savePrinterParameters(aprinter);

}

int myPrinter::openPrinterParameters(QString str){
    if(QFile(str).exists()){
        RewritePrinterSettings rwXml;
        rwXml.setFile(str, QIODevice::ReadOnly);
        listMap->clear();
        *listMap = rwXml.readXml();

        for(int i = 0; i < listMap->size(); i++){

            int copy = listMap->at(i).find("Copy").value().toInt();
            int doubleSidedPrinting = listMap->at(i).find("DoubleSidedPrinting").value().toInt();
            int orientation = listMap->at(i).find("Orientation").value().toInt();
            QString pageSize = listMap->at(i).find("PageSize").value();
            QString printerName = listMap->at(i).find("PrinterName").value();

            qDebug() << printer->printerName();

            if(printerName == "")
                printer->setPrinterName(printerName);

            // Опеределяем, произвольный ли это формат
            QVector<QString> vectorStr = customPaper(pageSize);
            if(vectorStr.size() == 1)
                printer->setPaperName(pageSize);
            else if (vectorStr.size() == 2)
                printer->setPaperSize(QSizeF(vectorStr[0].toDouble(), vectorStr[1].toDouble()), QPrinter::Millimeter);

            printer->setCopyCount(copy);
            printer->setDoubleSidedPrinting(doubleSidedPrinting);

            if(orientation){// Альбомное
                printer->setOrientation(QPrinter::Landscape);
            }else{// Портретное
                printer->setOrientation(QPrinter::Portrait);
            }
        }
    }
    return 0;
}
// Определение формата бумаги (либо зарезервированный либо произвольный)
QVector<QString> myPrinter::customPaper(QString str){
    QVector<QString> vectorStr;
    if(str.contains("Custom", Qt::CaseInsensitive)){
        if(str.contains("mm", Qt::CaseInsensitive)){
            int i = 7;
            int j = str.indexOf("x");
            vectorStr.append(str.mid(i,j-i));
            i = j+1;
            j = str.indexOf("mm");
            vectorStr.append(str.mid(i,j-i));
        }
    }else
        vectorStr.append(str);
    return vectorStr;
}
