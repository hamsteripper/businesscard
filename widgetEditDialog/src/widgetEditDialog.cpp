#include "widgetEditDialog/include/WidgetEditDialog.h"
// Конструктор
WidgetEditDialog::WidgetEditDialog(){

    frameLayout = new QHBoxLayout(this);
    vBoxRight = new QVBoxLayout;
    // Название элемента
    lable_widgetName = new QLabel("Имя   ");
    widgetName = new QLineEdit();
    // Текст элемента
    lable_text = new QLabel("Текст");
    text = new QLineEdit();
    // Виджет редактирования шрифтов
    fd = new QFontDialog();
    fd->setOption(QFontDialog::NoButtons);

    ok = new QPushButton("Принять");
    cancel = new QPushButton("Отмена");

    createConnectors();
    createLayout();
}
// Получение шрифта выбраного виджета
void WidgetEditDialog::receiveParametersOfWidget(ParametersOfWidget param){
    initialParam = param;
    this->currentParam = param;
    this->fd->setCurrentFont(currentParam.font);
    this->widgetName->setText(currentParam.userName);
    this->text->setText(currentParam.str);
}
// Изменение шрифта виджета
void WidgetEditDialog::parametersOfWidgetIsChange(QFont font){
    currentParam.font = font;
    emit sendChangedParametersOfWidget(currentParam);
}
// Обработчик закрытие окна
void WidgetEditDialog::closeEvent(QCloseEvent *_e){
    emit closeCardMainWindow(_e);
}
//
void WidgetEditDialog::createConnectors(){
    QObject::connect(fd, SIGNAL(currentFontChanged(QFont)), this, SLOT(parametersOfWidgetIsChange(QFont)));
    QObject::connect(widgetName, SIGNAL(textChanged(QString)), this, SLOT(nameChange(QString)));
    QObject::connect(text, SIGNAL(textChanged(QString)), this, SLOT(textChange(QString)));

    QObject::connect(ok, SIGNAL(clicked(bool)), this, SLOT(okClick(void)));
    QObject::connect(cancel, SIGNAL(clicked(bool)), this, SLOT(cancelClick(void)));
}
//
void WidgetEditDialog::okClick(){
    this->hide();
}
//
void WidgetEditDialog::cancelClick(){
    this->hide();
}
//
void WidgetEditDialog::nameChange(QString name){
    this->currentParam.userName = name;
    emit sendChangedParametersOfWidget(currentParam);
}
//
void WidgetEditDialog::textChange(QString str){
    this->currentParam.str = str;
    emit sendChangedParametersOfWidget(currentParam);
}
//
void WidgetEditDialog::createLayout(){

    // Text Size Layout
    QHBoxLayout* hL0 = new QHBoxLayout;
    hL0->addWidget(lable_widgetName);
    hL0->addWidget(widgetName);

    QHBoxLayout* hL1 = new QHBoxLayout;
    hL1->addWidget(lable_text);
    hL1->addWidget(text);

    QHBoxLayout* hL2 = new QHBoxLayout;
    hL2->addWidget(fd);

    QHBoxLayout* hL3 = new QHBoxLayout;
    hL3->addWidget(ok);
    hL3->addWidget(cancel);

    vBoxRight->addLayout(hL0);
    vBoxRight->addLayout(hL1);
    vBoxRight->addLayout(hL2);
    vBoxRight->addLayout(hL3);

    frameLayout->addLayout(vBoxRight);
    this->setLayout(frameLayout);
}
