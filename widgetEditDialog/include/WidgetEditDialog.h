#ifndef WIDGETEDITDIALOG_H
#define WIDGETEDITDIALOG_H

#include <QMainWindow>
#include <QObject>
#include <QString>
#include <QTextEdit>
#include <QListWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include "include/inputoutput.h"
#include <QSlider>
#include <QLabel>
#include <QFontComboBox>
#include <QSpinBox>
#include <QFontDialog>
#include "include/parametersofwidget.h"

class WidgetEditDialog : public QWidget{
    Q_OBJECT
private:
    QHBoxLayout* frameLayout;
    ///
    QVBoxLayout* vBoxRight;
    QLabel* lable_widgetName;
    QLineEdit* widgetName;
    QFontDialog* fd;
    QLabel* lable_text;
    QLineEdit* text;
    QPushButton* ok;
    QPushButton* cancel;
    ParametersOfWidget initialParam;
    ParametersOfWidget currentParam;
public:
    WidgetEditDialog();
private:
    void closeEvent(QCloseEvent *_e);
    void createLayout();
    void createConnectors();
signals:
    void closeCardMainWindow(QCloseEvent *_e);
    void sendChangedParametersOfWidget(ParametersOfWidget);
public slots:
    void receiveParametersOfWidget(ParametersOfWidget);
private slots:
    void parametersOfWidgetIsChange(QFont);
    void nameChange(QString);
    void textChange(QString);
    void okClick(void);
    void cancelClick(void);

};

#endif // WIDGETEDITDIALOG_H
